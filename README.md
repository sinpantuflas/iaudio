# Companion Code for WhaleNET

Please add the following folders in the path:

data/
data/whales-experiment

also remember to download kaggle's training set and change the directory location in getKaggleData_test.m

email: carlosa@deobaldia.com


Disclaimer: 

This folder contains source code from the NVIDIA Parallel Forall Blog post [Deep Learning for Computer Vision with MATLAB](http://nvda.ly/TSOT0) by Shashank Prasanna (The Mathworks).

## License

These examples are released under the BSD open source license.  Refer to license.txt in this directory for full details.
