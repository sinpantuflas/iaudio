%function plotting_clicks

colors = colormap(lines(128));
C = 'Color';
DN = 'DisplayName';
normal = 1;
filteringtype = 'zerophase';

%Original signal and transcient enhanced
figure(10), clf;
times = (0:(length(sig_ori)-1))./fs;
plot(times, sig_ori,C,colors(1,:),DN,'Original');
hold on
plot(times, sig_enh, C, colors(2,:),DN,'Enhanced');
title('Transient enhancement');
legend show
axis([times(1) times(end) -1 1]);

%Extracted click from original signal and transient enhanced. Spectrum
dim = 1e3;
snippet = sig_ori(click_ranges(select,1):click_ranges(select,3));
win1 = window(@hanning,length(snippet)); snippetW = snippet.*win1;
snippet2 = sig_enh(click_ranges(select,1):click_ranges(select,3));
win2 = window(@hanning,length(snippet2)); snippet2W = snippet2.*win2;

%Filtered spectra
rg = [0 100 110 160 170 200]; 
rg = rg./(fs/(2*1e3)); %Normalized frequency range
aa = [0 0 1 1 0 0];

B = firls(128, rg, aa,[1 1 1]);
switch filteringtype
    case 'normal'
%Normal forward path FIR filter
sf = filter(B,1,snippet);  s2f = filter(B,1,snippet2);
    case 'zerophase'
%Zero phase filtering (using cascaded filters)
sf = filtfilt(B,1,snippet);  s2f = filtfilt(B,1,snippet2);
    otherwise
        sf = []; s2f = [];
end

if normal,
    snippet = snippet./max(abs(snippet));
    snippet2 = snippet2./max(abs(snippet2));
    sf = sf./max(abs(sf));
    s2f = s2f./max(abs(s2f));
end

S = 20*log10(abs(fft(snippetW, 2^nextpow2(length(snippet)))));
S2 =  20*log10(abs(fft(snippet2W, 2^nextpow2(length(snippet)))));
S = S(1:floor(length(S)/2));
S2 = S2(1:floor(length(S2)/2));

SF =  20*log10(abs(fft(sf.*win1, 2^nextpow2(length(snippet)))));
S2F =  20*log10(abs(fft(s2f.*win2, 2^nextpow2(length(snippet2)))));
SF = SF(1:floor(length(SF)/2));
S2F = S2F(1:floor(length(S2F)/2));

%Apply zero phase filtering

figure(12), clf;
freqs = linspace(1,fs/2,max([length(S) length(S2)]));
times = (0:(max([length(snippet) length(snippet2)])-1))./fs;
subplot 211, 
plot(times(1:length(snippet)), snippet, C, colors(1,:), DN, 'Original'); hold on
plot(times(1:length(snippet2)), snippet2, C, colors(2,:), DN, 'Enhanced');
plot(times(1:length(snippet2)), sf, C, colors(3,:), DN, 'Original filtered');
plot(times(1:length(snippet2)), s2f, C, colors(4,:), DN, 'Enhanced filtered');
axis([ times(1), times(min([length(snippet), length(snippet2)])), -1 1]);
legend show
title('An extracted click segment')
subplot 212,
plot(freqs./dim, S, C, colors(1,:), DN, 'Original'); hold on
plot(freqs./dim, S2, C, colors(2,:), DN, 'Enhanced'); hold on
plot(freqs./dim, SF, C, colors(3,:), DN, 'Original Filtered'); hold on
plot(freqs./dim, S2F, C, colors(4,:), DN, 'Enhanced Filtered'); hold on
legend show
title('Power spectrum')
axis([freqs(1)./dim freqs(end)./dim 1.1*min([min(S2) min(S)]) 1.1*max([max(S) max(S2)])]);