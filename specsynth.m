function y = specsynth(spec)
% This generates a sound waveform from a spectrogram. SPEC is the input
% spectrogram, fs is the desired sample frequency, winsize is duh and
% overlap everybody knows its equal to winsize minus hopsize
% (c) libcom.org
%	D. Griffin and J. Lim. Signal estimation from modified 
%	short-time Fourier transform. IEEE Trans. Acoust. Speech 
%	Signal Process., 32(2):236-243, 1984.

winsize = 4096;
frameStep = 64;

%Linear vector mapping
%minv = -144.2813;
%maxv = 3.8235;
%spec2 = minv + (maxv-minv).*spec;
fftlen=4096;
w1= hamming(winsize);
w2= hamming(fftlen);
%w2=w1;
%w1 = window(winsize, '@hamming');
%w2 = window(fftlen, '@hamming');

% Calculate the length of the sound file,
slength = size(spec,2).*(winsize-(winsize-frameStep));
y(slength+2*winsize) = 0; y=y(:);

pend = size(spec,2);
pin = 0; pout=0;
while pin < pend,
    bin = spec(:,pin+1);
   
    %bin = bin.*exp(1i*2*pi*rand(size(bin)));
    %bin(2:end-1) = bin(2:end-1).*exp(1i*2*pi*rand(size(bin,1)-2,1));
    %bin(2:end-1) = bin(2:end-1).*exp(1i*phase(2:end-1,pin+1));
    
    bin = [bin;conj(flipud(bin(2:end-1)))];
    
    grain = fftshift(real(ifft(bin)));
    
    %grain = decimate(grain, fftlen/winsize);
    
    %grain1 = grain(1:winsize);
    
    y(pout+1:pout+winsize) = y(pout+1:pout+winsize) + grain.*w2;
    
    pin = pin + 1;
    
    pout = pout + frameStep;
end