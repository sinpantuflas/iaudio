function exerciseX(varargin)
% This function creates an example of a convolutional network for
% the problem of click/sweep/burst whale signal separation in maritime 
% environments.

addpath(genpath('./data/'));
addpath(genpath('./vlfeat/'));
addpath(genpath('./matconvnet/'));

setup;
isRGB=false;

%[imdb tstdb] = getKaggleData_testv2_S(isRGB);
load('images_smirnov');

% In this example, 20% the evaluation set is set out for testing
% From the remaining , 70% is used for training and the rest 30% for
% validating.
%imdb2 = load('data/kaggleImagesTest_rev.mat');
%imdb = imdb2; clear imdb2;
imdb.images.label(find(imdb.images.label==0))=2;

%imdb = load('data/kaggleImagesTest_cleaned.mat');

%disp('Getting Kaggle Data')
%[imdb tstdb] = getKaggleData_test();
%save('./data/kaggleImages_trn.mat','-struct','imdb','-v7.3');
%save('./data/kaggleImages_eva.mat','-struct','tstdb','-v7.3');

% Plot the positive examples.
%figure(10) ; clf ; colormap gray ;
%subplot(1,2,1) ;
%vl_imarraysc(imdb.images.data(:,:,imdb.images.label==2 & imdb.images.set==1)) ;
%axis image off ;
%title('Training spectrums for NARW calls');
%subplot(1,2,2) ;
%vl_imarraysc(imdb.images.data(:,:,imdb.images.label==2 & imdb.images.set==2)) ;
%axis image off ;
%title('Validation spectrums for NARW calls');

% figure(11) ; clf ; colormap gray ;
% vl_imarraysc(imdb.images.data(:,:,imdb.images.label==2 & imdb.images.set==3)) ;
% axis image off ;
% title('Testing spectrums for NARW calls');

% % Balance sets
% indexes1 = find(imdb.images.label==1);
% indexes2 = find(imdb.images.label==2);
% a = randi(length(indexes1),1, ceil(1.3*length(indexes2)));
% indexes1(a)=0;
% away=indexes1(find(indexes1));
% imdb.images.id(away)=[];
% imdb.images.set(away)=[];
% imdb.images.label(away)=[];
% imdb.images.data(:,:,away)=[];

%Shuffle
shuffleSet = true;
if shuffleSet,
ix=randperm(length(imdb.images.label));
imdb.images.id=imdb.images.id(ix);
imdb.images.set=imdb.images.set(ix);
imdb.images.label=imdb.images.label(ix);
if ~isRGB,
imdb.images.data=imdb.images.data(:,:,ix);
else
imdb.images.data=imdb.images.data(:,:,:,ix);
end
end
%imdb.images.label(imdb.images.label==0)=2;
% -------------------------------------------------------------------------
% Part 4.2: initialize a CNN architecture
% -------------------------------------------------------------------------

net = initializeWhaleCNN_smirnov(isRGB);
%net = initializeWhaleCNN_2(false);
save('indexed_terms_imdb','ix');
vl_simplenn_display(net);

isSpatialLabel = false;
if isSpatialLabel,

libl = zeros(11,11,numel(imdb.images.id));
ELF=ones(11,11);
ix1=find(imdb.images.label==1);
for n=1:length(ix1),
libl(:,:,ix1(n))=1.*ELF;
end
ix0=find(imdb.images.label==2);
for n=1:length(ix0),
libl(:,:,ix0(n))=2.*ELF;
end
libl = reshape(libl, [11 11 1 numel(imdb.images.id)]);
imdb.images.label=libl;
end
% -------------------------------------------------------------------------
% Part 4.3: train and evaluate the CNN
% -------------------------------------------------------------------------
tic
trainOpts.batchSize = 100;
trainOpts.numEpochs = 25;
trainOpts.continue = true ;
trainOpts.useGpu = false ;
lr=0.05;
for n=1:(trainOpts.numEpochs-1),
lr=[lr lr(end)/1.10004];
end

trainOpts.learningRate = lr ;
trainOpts.expDir = 'data/whales-experiment' ;
%trainOpts = vl_argparse(trainOpts, varargin);

% Take the average image out
imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ; %THIS IS REDUNDANT WHATEVER

% Convert to a GPU array if needed
if trainOpts.useGpu
  imdb.images.data = gpuArray(imdb.images.data) ;
end

%Convert images to single
imdb.images.data = single(imdb.images.data);

%********* CDO: ERROR
% Error in stroke_nn_medicalData_011415 (line 76)
% [net, info] = cnn_train(net, medical_record_imdb, @getBatch, ...
% >>>> A better way is to set labels as +1 and -1, and set opts.errorFunction = 'binary' ; in the cnn_train file.

% Call training function in MatConvNet
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts) ;
%[net,info] = cnn_train(net, imdb, @getBatchWithJitter, trainOpts) ;

toc
% Move the CNN back to the CPU if it was trained on the GPU
if trainOpts.useGpu
  net = vl_simplenn_move(net, 'cpu') ;
end

disp('Saving the net');

%% Save the result for later use
net.layers(end) = [] ;
net.imageMean = imageMean;
save('data/whales-experiment/whalecnn_rev.mat', '-struct', 'net') ;

% -------------------------------------------------------------------------
% Part 4.4: visualize the learned filters
% -------------------------------------------------------------------------

%figure(2) ; clf ; colormap gray ;
%vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2)
%axis equal ; title('Filters in the first layer of the CNN') ;

%% -------------------------------------------------------------------------
% Part 4.5: apply the model
% -------------------------------------------------------------------------
% Get samples
%tstdb = getKaggleData_test();
%save('./data/kaggleImagesTest.mat','-struct','tstdb','-v7.3');

%tstdb = load('./data/kaggleImagesTest_eval.mat');

% Try other data,
%tstdb = getWhaleData(fileName);

% Perform click / sweep classification
% Extract just continous regions in the spectra
% Denoising, normalization, equalization, binariyation,
% Continous region detection
% - Directivity 
% - Frequency range

% Load the CNN learned before
net = load('data/whales-experiment/whalecnn_rev.mat') ;
net.layers{end+1} = struct('type', 'softmax') ;

tstdb.images.data = tstdb.images.data - net.imageMean ;
tstdb.images.label(find(tstdb.images.label==0))=2;
%vecPred=zeros(2,size(tstdb.images.id,2));

% Apply the CNN 
%h = waitbar(0,'Please wait');


n = 1;
poll = cell(numel(tstdb.images.id),1);
for n=1:numel(tstdb.images.id),
             %   waitbar(n/numel(tstdb.images.id),h,'Please wait');
    %res = vl_simplenn(net, single(tstdb.images.data(:,:,n)),[],[]) ;
    %res = vl_simplenn(net, single(imresize(tstdb.images.data(1:72,1:72,n),[36 36])),[],[]) ;
    res = vl_simplenn(net, single(imresize(tstdb.images.data(:,:,n),[36 36])),[],[]) ;
    %res = vl_simplenn(net, single(tstdb.images.data(:,:,:,n)),[],[]) ;
    %[e f] = max(max(max(res(end).x)));
    [e f] = max(res(end).x);
    poll{n} = [e, f];
end
%close(h)
r=cell2mat(poll);
rGT=tstdb.images.label;
rGT(rGT==0)=2;
rTS=r(:,2);
 
save('./data/result_map.mat','poll','rGT','rTS');
ConfusionMatrix=confusionmat(rGT,double(rTS'))
CM=ConfusionMatrix; clear ConfusionMatrix
Accuracy=100*(CM(1,1)+CM(2,2))/sum(CM(:)) %Accuracy
Precision=100*(CM(2,2)/(CM(2,2)+CM(1,2))) %Precision (for clicks)
MissRate=100*(CM(2,1)/(CM(2,1)+CM(2,2))) %Miss Rate (for clicks)


pause();

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
%im = imdb.images.data(:,:,:,batch) ;
%im = imresize(im, [64 64]) ;
%im = 256 * reshape(im, 64, 64, 1, []) ;

% First: fix the image size
im = reshape(im, 36, 36, 1, []) ;%

% Since labels are two-dimensional
labels = imdb.images.label(1,batch) ;
%labels = imdb.images.label(:,:,1,batch) ;

% --------------------------------------------------------------------
function [im, labels] = getBatchWithJitter(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
labels = imdb.images.label(1,batch) ;

n = numel(batch) ;
train = find(imdb.images.set == 1) ;

sel = randperm(numel(train), n) ;
im1 = imdb.images.data(:,:,sel) ;

sel = randperm(numel(train), n) ;
im2 = imdb.images.data(:,:,sel) ;

ctx = [im1 im2] ; %If it is 32, then 17 so if it is 64 then 33 until 96
ctx(:,33:96,:) = min(ctx(:,33:96,:), im) ;

dx = randi(11) - 6 ;
im = ctx(:,(33:96)+dx,:) ;
sx = (33:96) + dx ;

dy = randi(5) - 2 ;
sy = max(1, min(64, (1:64) + dy)) ;

im = ctx(sy,sx,:) ;

% Visualize the batch:
% figure(100) ; clf ;
% vl_imarraysc(im) ;

%im = 256 * reshape(im, 64, 64, 1, []) ;
im = reshape(im, 64, 64, 1, []) ;

function [im, labels] = getBatchWithJitter2(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
labels = imdb.images.label(1,batch) ;

n = numel(batch) ;
train = find(imdb.images.set == 1) ;

sel = randperm(numel(train), n) ;
im1 = imdb.images.data(:,:,sel) ;

sel = randperm(numel(train), n) ;
im2 = imdb.images.data(:,:,sel) ;

ctx = [im1 im2] ; %If it is 32, then 17 so if it is 64 then 33 until 96
ctx(:,33:96,:) = min(ctx(:,33:96,:), im) ;

dx = randi(11) - 6 ;
im = ctx(:,(33:96)+dx,:) ;
sx = (33:96) + dx ;

dy = randi(5) - 2 ;
sy = max(1, min(64, (1:64) + dy)) ;

im = ctx(sy,sx,:) ;

% Visualize the batch:
% figure(100) ; clf ;
% vl_imarraysc(im) ;

%im = 256 * reshape(im, 64, 64, 1, []) ;
im = reshape(im, 64, 64, 1, []) ;