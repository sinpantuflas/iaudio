clear all;

addpath(genpath('./'));

%nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/car/15dB/';
%fileName = 'sp07_car_sn15.wav';

%nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/car/0dB/';
%fileName = 'sp07_car_sn0.wav';

%nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/babble/0dB/';
%fileName = 'sp07_babble_sn0.wav';

nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/babble/15dB/';
fileName = 'sp07_babble_sn15.wav';

%nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/exhibition/0dB';
%fileName = 'sp07_exhibition_sn0.wav';
% 
% nDirC = '/home/obaldia/Proyectos/Sounds/Noizeus/clean/';
% fileNC = 'sp07.wav';
% 

% % Read input signal
% [a fs] = audioread(strcat(nDir,fileName));
% a = a(:,1); %Take only left channel.
% a = (a - mean(a))./std(a);
% a = a./max(abs(a));
% 
% [c fs] = audioread(strcat(nDirC,fileNC));
% c = c(:,1); %Take only left channel.
% c = (c - mean(c))./std(c);
% c = c./max(abs(c));

% Calculate the spectrum
nfft_s = 2048;
win_s = 512;
olap_s = floor(win_s - win_s/8);
noiseTh = -80;

% Perform de-noising and superresolution.
%netDir = 'tiers/';
%thisNet = 'net-dnoiz-B-09-27-08-37.mat'; %Babble

load('this_firstsig0db/net-epoch-500.mat')
this_net = dagnn.DagNN.loadobj(net);




%#Tier two
%netDir = './learned/';

%thisNet = 'net-dnoiz-reas-1-06-28-17-34.mat';
%thisNet = 'net-dnoiz-reas-1-06-29-14-07.mat';
%thisNet = 'net-dnoiz-reas-2-06-28-17-54.mat';
%thisNet = 'net-dnoiz-reas-3-06-28-18-13.mat';
%thisNet = 'net-dnoiz-reas-4-06-28-18-33.mat';
%thisNet = 'net-dnoiz-reas-5-06-28-18-53.mat';
%thisNet = 'net-dnoiz-reas-6-06-28-19-13.mat';
%thisNet = 'net-dnoiz-reas-7-06-28-19-33.mat';

%Load the network
net=load(strcat(netDir,thisNet));
net = vl_simplenn_tidy(net);
net.layers(end)=[];
vl_simplenn_display(net)

addpath('../.');


% A file streamer that checks for fs.
currDirName = 'path/to/route/';

% Get input signals contained in the folder (the folder should contain ONLY
% wav/mp3/aac files)
currDir = dir(currDirName);
opts.denoiseANT = 1; fid=0;
for n0 = 1:size(currDir,1)-2,
    if ~currDir(n0).isdir, %Do not read subdirectories
        %File ID/Counter
        fid = fid + 1;
        
        % To be applied for longer signals
        streamLength = 8; %Length of the stream in seconds
        streamOverlap = 0; %Seconds of overlapping between stream buffers
        
        isReading = true; %Flag for continuing reading samples
        nextSample = 1;
        chunk = 0;
        while isReading,
            
            % Load information from audio file.
            aInfo = audioinfo(strcat(currDirName,currDir(n0).name));
            
            fs = aInfo.SampleRate; % Ankit: this is the line which reads the sampling frequency before reading the file
            
            if (aInfo.TotalSamples./aInfo.SampleRate >= streamLength)
                if ~((nextSample + streamLength.*aInfo.SampleRate - 1)>aInfo.TotalSamples),
                    [a fs] = audioread(strcat(currDirName,currDir(n0).name),[nextSample, nextSample + streamLength.*aInfo.SampleRate - 1]);
                else
                    [a fs] = audioread(strcat(currDirName,currDir(n0).name),[nextSample, aInfo.TotalSamples]);
                    % Next sample in loop.
                end
                %For the next read
                prevSample = nextSample;  %Read pointers (We have two: the start and the end of the chunks
                nextSample = streamLength.*aInfo.SampleRate + prevSample;
            else
                % We dont have to divide the sream
                [a fs] = audioread(strcat(currDirName,currDir(n0).name),'double');
                prevSample = 1;
                nextSample = prevSample + length(a);
                
            end
            disp(strcat('Reading file number +', num2str(fid)));
            disp(strcat('fs=',num2str(aInfo.SampleRate./1000),'kHz. From sample +',num2str(prevSample)));
            
            % Magic happens here
            
            
            
            
            
            %To do: change meta input size
            sz = [1024 50];
            
            alpha = 1; beta = 0.6;
            
            Lk = 2.^nextpow2(floor(0.5.*fs));
            hopSize = Lk/8;
            
            pend = length(a) - Lk;
            pin = 0; pout= 0;
            y=zeros(length(a)+Lk,1);
            tic
            while pin<pend
                n=pin;
                
                try
                    block = a(pin+1:pin+Lk).*window(@hann,Lk);
                catch
                    block = zeros(Lk,1);
                    block(1:end) = a(pin+1:pin+Lk);
                end
                
                [spC fp tp] = spectrogram(block,win_s,olap_s,nfft_s, fs);
                sz1=size(spC);      val = max(max(abs(spC)));
                AnglB = angle(spC);
                nVal = max(max(abs(spC)));
                sp=abs(spC)./nVal;
                spL = 20*log10(abs(sp));
                spL(spL<noiseTh)=noiseTh;
                
                % Re-assigned spectrogram
                warning('off','all')
                [Rsp Rfp Rtp] = raspecgram2(a,nfft_s,fs,win_s,olap_s);
                % from ratoimage
                Rsp = log10(abs(Rsp)./max(max(abs(Rsp))));
                Rsp(Rsp<-8)=-8;
                Rsp = mat2gray(Rsp);
                
                
                bypass = 0;
                if ~bypass,
                    inSp = mat2gray(spL);
                    inSp = imresize(inSp, sz);
                    inSp = single(inSp);
                    inOW = im2bw(inSp,0.7); %Changed to the new arch
                    im(:,:,1)=inSp;
                    im(:,:,3)=inOW;
                    inRp = imresize(Rsp, sz);
                    inRp = single(inRp);
                    im(:,:,2)=inRp;
                    
                    [res err1 err2 err3] = vl_simplenn_regression(net, im, [],[],'mode','test');
                    
                    dmask=imresize(res(end).x./max(max(abs(res(end).x))),sz1);
                    dmask=mat2gray(dmask); %Fixed an issue
                    
                    %h = 1/3*ones(9); %<-- crazy effect
                    h = 1/3*ones(3);
                    Zsmooth = filter2(h,dmask);
                    %figure(3); subplot(1,2,1); imagesc(dmask); subplot(1,2,2); imagesc(Zsmooth)
                    dmask = Zsmooth;
                    
                    
                    %CDOP
                    %v1.
                    %dmask=-dmask;
                    %dmask(dmask<0)=1;
                    %v2.:
                    %sp2=alpha.*(spL-abs((dmask).*spL));
                    %sp2=alpha.*(((dmask).*spL));
                    %sp2=sp2./max(max(abs(sp2)));
                    %sp2(sp2<noiseTh) = noiseTh;
                    
                    %v1.
                    %D = abs(spC).*(dmask);
                    %v2.
                    %D = 10.^(sp2/20); D=(D./max(max(abs(D)))).*val;
                    %v3.
                    %D = spL - abs((beta.*dmask).*(spL));
                    %M = filter2(h,(dmask).*(spL));
                    D = spL - beta.*(((dmask).*(spL)).^2);
                    
                    %v4.
                    %msk = dmask.*sp; spM = 20*log10(msk);
                    %D = spL + spM; D=spM;
                    %D = D - max(max(D));
                    
                    
                    D(D<noiseTh)=noiseTh;
                    D = (10.^(D/20)).*nVal;
                    
                    %AnglB = unwrap(AnglB);
                    X = D.*(cos(AnglB) + 1i*sin(AnglB));
                else
                    D = (10.^(spL./20)).*nVal;
                    AnglB = unwrap(AnglB);
                    X = D.*(cos(AnglB) + 1i*sin(AnglB));
                    %X=spC;
                end
                
                %figure(9); subplot(1,2,1), imagesc(abs(spC)); colorbar, subplot(1,2,2), imagesc(abs(D)), colorbar;
                
                %grain = (fftshift((real(ifft(X,Nfft)))));
                grain = invspecgram(X,nfft_s,fs,win_s,olap_s);
                grain = grain(:);
                
                y(pout+1:pout+length(grain)) = y(pout+1:pout+length(grain)) +...
                    grain.*window(@hann,Lk);%.*window(@hann,length(grain));
                
                pin = pin + hopSize;
                pout = pout + hopSize;
            end
            y(isnan(y)) = 0;
            y = y./max(abs(y));
            toc
            figure(6); clf, CN='DisplayName';
            plot(a,CN,'Original'), hold on,
            plot(a-y(1:length(a)),'--',CN,'Error');
            plot(y,CN,'Processed'),
            legend show
            
            SNR_Before = snr(a,a-c)
            SNR_After = snr(y(1:length(c)),(y(1:length(c))-c))
            
            audiowrite(strcat('output/',fileName(1:end-4),'_dz.wav'),y,fs)
            
            pause(0.1)
        end
    end
end


% for n0 = 1:size(currDir,1),
%     if ~currDir(n0).isdir,
%
%
%         % Calculate the spectrum
%         nfft_s = 2048;
%         win_s = 512;
%         olap_s = floor(win_s - win_s/8);
%         noiseTh = -80;
%
%         [sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
%         spL = 20*log10(abs(sp)); spL(spL<noiseTh)=noiseTh;
%         spL=abs(sp)./max(max(abs(sp)));
%         inSp = mat2gray(spL);
%
%         % Extract the images, 1024 times 300;
%         ix = 100; iy = 600; n1 = 1;
%         pin = 0; pend = size(inSp,2) - ix; ims=[];
%         while pin < pend,
%             ims(:,:,n1) = imresize(inSp(:,pin+1:pin+ix),[ix iy]);
%             pin = pin + ix; n1=n1+1;
%         end
%         if ~isempty(ims),
%         im{n2} = ims; n2=n2+1;
%         end
% end %if isdir
% end %all dir files
%
%
% pause()s

% a = resample(a,fsn,fs); fs = fsn;
%
% % Calculate the spectrum
% nfft_s = 4096;
% win_s = 512;
% olap_s = floor(win_s - win_s/8);
% noiseTh = -45;
%
% [sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
% %spL = 20*log10(abs(sp)); %spLN = spL;
% spL=abs(sp)./max(max(abs(sp)));
% inSp = mat2gray(spL);
% inSp = imresize(inSp,[300 500],'bicubic');
% %spL(spL<=noiseTh) = noiseTh;

% Perform de-noising and superresolution.
% netDir = 'weightsandbiases/';
% thisNet = 'net-epoch-260_denoise2.mat';
% load(strcat(netDir,thisNet));
% net = vl_simplenn_tidy(net);
% addpath(netDir);
%
% inSp = single(inSp);
% inBW = edge(inSp,'log');
% inOW = im2bw(inSp,0.9);
% im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
% im = single(im);
% net.layers(end)=[];
% vl_simplenn_display(net)
% [res err1 err2 err3] = vl_simplenn_purba(net, im, 0, [],[],'mode','test');
%
% dmask=imresize(res(end).x./max(max(abs(res(end).x))),[size(sp,1),size(sp,2)]);
% denoised = (dmask.*sp);
%
% out = specsynth(denoised);
% out2=invspecgram(denoised./max(max(abs(denoised))),nfft_s,fs,win_s,olap_s);
%
% figure;
% subplot 121
% imagesc(tp,fp,20*log10(abs(sp./max(max(abs(sp))))));
% axis xy
% title('Original');
% xlabel('Time');
% ylabel('Frequency');
%
% subplot 122
% imagesc(tp,fp,20*log10(abs(denoised./max(max(abs(denoised))))));
% axis xy
% title('Denoised');
% xlabel('Time');
% ylabel('f \rightarrow');
