function denoiz_train_all()

%First step is to read the wavefiles and generate a imdb

cleanDir = '/home/obaldia/Proyectos/Sounds/Noizeus/clean/';
n15Dir = '/home/obaldia/Proyectos/Sounds/Noizeus/car/15dB/';

n00Dir1 = '/home/obaldia/Proyectos/Sounds/Noizeus/car/0dB/';
n00Dir2 = '/home/obaldia/Proyectos/Sounds/Noizeus/street/0dB/';
n00Dir3 = '/home/obaldia/Proyectos/Sounds/Noizeus/exhibition/0dB/';
n00Dir4 = '/home/obaldia/Proyectos/Sounds/Noizeus/station/0dB/';
n00Dir5 = '/home/obaldia/Proyectos/Sounds/Noizeus/babble/0dB/';
n00Dir6 = '/home/obaldia/Proyectos/Sounds/Noizeus/restaurant/0dB/';
n00Dir7 = '/home/obaldia/Proyectos/Sounds/Noizeus/airport/0dB/';

im1 = makedata(cleanDir);
%im{1} = makedata(cleanDir);
%im2 = makedata(n15Dir);
imm{1} = makedata(n00Dir1);
imm{2} = makedata(n00Dir2);
imm{3} = makedata(n00Dir3);
imm{4} = makedata(n00Dir4);
imm{5} = makedata(n00Dir5);
imm{6} = makedata(n00Dir6);
imm{7} = makedata(n00Dir7);



for tr=7:7,

    im3 = imm{tr};
    
imdb.data = []; n00=1;
for n0=1:size(im1,2),
    tmpcell = im3{n0};
    tmpcelll= im1{n0};
    for n1=1:size(tmpcell,3),
    inSp = single(tmpcell(:,:,n1));
    inBW = edge(inSp,'log');
    inOW = im2bw(inSp,0.9);
    im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
    imdb.images.data(:,:,:,n00)=im;
    imdb.images.id(n00) = n00;
    imdb.images.label(:,:,n00) = tmpcelll(:,:,n1);
    imdb.images.set(n00) = 1;
    n00 = n00 + 1;
    end
end
end
% 
% % Perform de-noising and superresolution.

%setup;

 netDir = 'weightsandbiases/';
 thisNet = 'net-epoch-260_denoise2.mat';
 load(strcat(netDir,thisNet));
 net = vl_simplenn_tidy(net);
 addpath(netDir);
% 

 tic
 trainOpts.batchSize = 5;
 trainOpts.numEpochs = 1000;
 trainOpts.continue = true ;
 %trainOpts.residualnet = 1;
 %trainOpts.useGpu = true ;
 lr=0.1; %changed from 0.5 22.04 %0.0007 and 2.0004
 for n=1:(trainOpts.numEpochs-1),
 lr=[lr lr(end)/1.00004];
 end
 
 %net.layers(end)=[];
 
 [net,info] = cnn_train(net, imdb, @getBatch, trainOpts);
% 
% trainOpts.learningRate = lr ;
% trainOpts.expDir = 'data/whales-experiment' ;
% 
% disp('Training the network...');
% 
% % Take the average image out
% imageMean = mean(imdb.images.data(:)) ;
% imdb.images.data = imdb.images.data - imageMean ; %THIS IS REDUNDANT WHATEVER
% 
% imdb.images.data = single(imdb.images.data);
% 
% 
% toc
% 
 disp('Saving the net'); timeStamp = datestr(now,'mm-dd-HH-MM');
 save(strcat('net-dnoiz-',tr,'-',timeStamp,'.mat'), '-struct', 'net') ;
cleanTrain;

end

function im = makedata(currDirName)
%%
% Current data
timeRes = 0.5; %The frame size in seconds
freqRes = 1000; %The frequency bandwidth

currDir = dir(currDirName);
im = cell(1); n2=1;
for n0 = 1:size(currDir,1)-2,
    if ~currDir(n0).isdir,
        % Read input signal
        [a fs] = audioread(strcat(currDirName, currDir(n0).name));
        a = a(:,1); %Take only left channel.
        a = (a - mean(a))./std(a);
        a = a./max(abs(a));

        % Calculate the spectrum
        nfft_s = 2048;
        win_s = 512;
        olap_s = floor(win_s - win_s/8);
        noiseTh = -80;

        [sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
        spL=abs(sp)./max(max(abs(sp)));
        spL = 20*log10(abs(sp)); spL(spL<noiseTh)=noiseTh;
        
        inSp = mat2gray(spL);
        
        % Extract the images, 1024 times 300;
        ix = 100; iy = 600; n1 = 1;
        pin = 0; pend = size(inSp,2) - ix; ims=[];
        while pin < pend,
            ims(:,:,n1) = imresize(inSp(:,pin+1:pin+ix),[ix iy]);
            pin = pin + ix; n1=n1+1;
        end
        if ~isempty(ims),
        im{n2} = ims; n2=n2+1;
        end
end %if isdir
end %all dir files

for n0=1:size(im,1),
    if isempty(im{n0}),
        im(n0)=[];
    end
end
end

function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
isRGB = false;

im = single(imdb.images.data(:,:,:,batch));
%im = imresize(im,[32 32 3]);
%im = reshape(im,32 ,32 , 3, []);
labels = imdb.images.label(:,:,batch);
labels=reshape(labels,size(labels,1),size(labels,2),1,[]);
end
