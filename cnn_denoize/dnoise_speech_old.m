clear all;

nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/car/15dB/';
fileName = 'sp01_car_sn15.wav';

nDir = '/home/obaldia/Proyectos/Sounds/Noizeus/car/0dB/';
fileName = 'sp07_car_sn0.wav';

addpath('../.');

% Read input signal
[a fs] = audioread(strcat(nDir,fileName));
a = a(:,1); %Take only left channel.
a = (a - mean(a))./std(a);
a = a./max(abs(a));

% Calculate the spectrum
nfft_s = 2048;
win_s = 512;
olap_s = floor(win_s - win_s/8);
noiseTh = -80;

% Perform de-noising and superresolution.
netDir = '../learned/denoiz/'; 
%thisNet = 'net-dnoiz-09-14-14-14.mat';
%thisNet = 'net-dnoiz-09-14-19-21.mat';
thisNet = 'net-dnoiz-09-15-22-01.mat';
net=load(strcat(netDir,thisNet));
net = vl_simplenn_tidy(net);
net.layers(end)=[];
vl_simplenn_display(net)

%To do: change meta input size 
sz = [1024 50];

alpha = 1;

hopSize = 512;
Lk = 0.5.*fs;

pend = length(a) - Lk,
pin = 0; pout= 0;
y=zeros(length(a)+Lk,1);

while pin<pend
    n=pin;
    
    try
    block = a(pin+1:pin+Lk);
    catch
    block = zeros(Lk,1);
    block(1:end) = a(pin+1:pin+Lk);
    end
    
    [spC fp tp] = spectrogram(block,win_s,olap_s,nfft_s, fs);
    sz1=size(spC);      val = max(max(abs(spC)));
    AnglB = angle(spC);
    sp=abs(spC)./max(max(abs(spC)));
    spL = 20*log10(abs(sp)); 
    spL(spL<noiseTh)=noiseTh;
    
    inSp = mat2gray(spL);
    inSp = imresize(inSp, sz); 
    inSp = single(inSp);
    inBW = edge(inSp,'log');
    inOW = im2bw(inSp,0.9);
    im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
    
    [res err1 err2 err3] = vl_simplenn_regression(net, im, [],[],'mode','test');
    
    dmask=imresize(res(end).x./max(max(abs(res(end).x))),sz1);
    dmask=mat2gray(dmask); %Fixed an issue
    
    %CDOP
    %v1.
    %dmask=-dmask;
    %dmask(dmask<0)=1;
    %v2.:
    sp2=alpha.*(spL-abs((dmask).*spL));
    %sp2=alpha.*(((dmask).*spL));
    %sp2=sp2./max(max(abs(sp2)));
    sp2(sp2<noiseTh) = noiseTh; 
    
    %v1.
    %D = abs(spC).*(dmask);
    %v2.
    D = 10.^(sp2/20); D=(D./max(max(abs(D)))).*val;
    
    AnglB = unwrap(AnglB);
    X = D.*(cos(AnglB) + +j*sin(AnglB)); 
    
    %grain = (fftshift((real(ifft(X,Nfft)))));
    grain = invspecgram(X,nfft_s,fs,win_s,olap_s);
    grain = grain(:);
   
    y(pout+1:pout+length(grain)) = y(pout+1:pout+length(grain)) +...
        grain.*window(@hann,length(grain));
    
    pin = pin + hopSize;
    pout = pout + hopSize; 
end
y = y./max(abs(y));
pause(0.1)



% for n0 = 1:size(currDir,1),
%     if ~currDir(n0).isdir,
%         
% 
%         % Calculate the spectrum
%         nfft_s = 2048;
%         win_s = 512;
%         olap_s = floor(win_s - win_s/8);
%         noiseTh = -80;
% 
%         [sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
%         spL = 20*log10(abs(sp)); spL(spL<noiseTh)=noiseTh;
%         spL=abs(sp)./max(max(abs(sp)));
%         inSp = mat2gray(spL);
%         
%         % Extract the images, 1024 times 300;
%         ix = 100; iy = 600; n1 = 1;
%         pin = 0; pend = size(inSp,2) - ix; ims=[];
%         while pin < pend,
%             ims(:,:,n1) = imresize(inSp(:,pin+1:pin+ix),[ix iy]);
%             pin = pin + ix; n1=n1+1;
%         end
%         if ~isempty(ims),
%         im{n2} = ims; n2=n2+1;
%         end
% end %if isdir
% end %all dir files
% 
% 
% pause()s

% a = resample(a,fsn,fs); fs = fsn;
% 
% % Calculate the spectrum
% nfft_s = 4096;
% win_s = 512;
% olap_s = floor(win_s - win_s/8);
% noiseTh = -45;
% 
% [sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
% %spL = 20*log10(abs(sp)); %spLN = spL;
% spL=abs(sp)./max(max(abs(sp)));
% inSp = mat2gray(spL);
% inSp = imresize(inSp,[300 500],'bicubic');
% %spL(spL<=noiseTh) = noiseTh;

% Perform de-noising and superresolution.
% netDir = 'weightsandbiases/';
% thisNet = 'net-epoch-260_denoise2.mat';
% load(strcat(netDir,thisNet));
% net = vl_simplenn_tidy(net);
% addpath(netDir);
% 
% inSp = single(inSp);
% inBW = edge(inSp,'log');
% inOW = im2bw(inSp,0.9);
% im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
% im = single(im);
% net.layers(end)=[];
% vl_simplenn_display(net)
% [res err1 err2 err3] = vl_simplenn_purba(net, im, 0, [],[],'mode','test'); 
% 
% dmask=imresize(res(end).x./max(max(abs(res(end).x))),[size(sp,1),size(sp,2)]);
% denoised = (dmask.*sp);
% 
% out = specsynth(denoised);
% out2=invspecgram(denoised./max(max(abs(denoised))),nfft_s,fs,win_s,olap_s);
% 
% figure;
% subplot 121
% imagesc(tp,fp,20*log10(abs(sp./max(max(abs(sp))))));
% axis xy
% title('Original');
% xlabel('Time');
% ylabel('Frequency');
% 
% subplot 122
% imagesc(tp,fp,20*log10(abs(denoised./max(max(abs(denoised))))));
% axis xy
% title('Denoised');
% xlabel('Time');
% ylabel('f \rightarrow');