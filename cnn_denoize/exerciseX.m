function exerciseX(varargin)
% This function creates an example of a convolutional network for
% the problem of click/sweep/burst whale signal separation in maritime 
% environments.

setup ;

%imdb = getKaggleData();
%save('./data/kaggleImages.mat','-struct','imdb','-v7.3');
%
%imdb = load('data/kaggleImages.mat');

% In this example, 20% the evaluation set is set out for testing
% From the remaining , 70% is used for training and the rest 30% for
% validating.
%imdb2 = load('data/kaggleImagesTest_rev.mat');
%imdb = imdb2; clear imdb2;

%imdb = load('data/kaggleImagesTest_cleaned.mat');

%[imdb tstdb] = getKaggleData_test();
%save('./data/kaggleImages_trn.mat','-struct','imdb','-v7.3');
%save('./data/kaggleImages_eva.mat','-struct','tstdb','-v7.3');

imdb = load('./data/kaggleImages_trn.mat');
tstdb = load('./data/kaggleImages_eva.mat');


% Plot the positive examples.https://www.facebook.com/BecciZa?fref=ts
figure(10) ; clf ; colormap gray ;
subplot(1,2,1) ;
vl_imarraysc(imdb.images.data(:,:,imdb.images.label==2 & imdb.images.set==1)) ;
axis image off ;
title('Training spectrums for NARW calls');
 
subplot(1,2,2) ;
vl_imarraysc(imdb.images.data(:,:,imdb.images.label==2 & imdb.images.set==2)) ;
axis image off ;
title('Validation spectrums for NARW calls');

% figure(11) ; clf ; colormap gray ;
% vl_imarraysc(imdb.images.data(:,:,imdb.images.label==2 & imdb.images.set==3)) ;
% axis image off ;
% title('Testing spectrums for NARW calls');

% -------------------------------------------------------------------------
% Part 4.2: initialize a CNN architecture
% -------------------------------------------------------------------------

net = initializeWhaleCNN() ;
vl_simplenn_display(net);

% -------------------------------------------------------------------------
% Part 4.3: train and evaluate the CNN
% -------------------------------------------------------------------------
tic
trainOpts.batchSize = 100 ;
trainOpts.numEpochs = 5 ;
trainOpts.continue = true ;
trainOpts.useGpu = false ;
trainOpts.learningRate = 0.001 ;
trainOpts.expDir = 'data/whales-experiment' ;
trainOpts = vl_argparse(trainOpts, varargin);

% Take the average image out
imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ;

% Convert to a GPU array if needed
if trainOpts.useGpu
  imdb.images.data = gpuArray(imdb.images.data) ;
end

%Convert images to single
imdb.images.data = single(imdb.images.data);

%********* CDO: ERROR
% Error in stroke_nn_medicalData_011415 (line 76)
% [net, info] = cnn_train(net, medical_record_imdb, @getBatch, ...
% >>>> A better way is to set labels as +1 and -1, and set opts.errorFunction = 'binary' ; in the cnn_train file.

% Call training function in MatConvNet
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts) ;

toc
% Move the CNN back to the CPU if it was trained on the GPU
if trainOpts.useGpu
  net = vl_simplenn_move(net, 'cpu') ;
end

%% Save the result for later use
net.layers(end) = [] ;
net.imageMean = imageMean ;
save('data/whales-experiment/whalecnn_rev.mat', '-struct', 'net') ;

% -------------------------------------------------------------------------
% Part 4.4: visualize the learned filters
% -------------------------------------------------------------------------

figure(2) ; clf ; colormap gray ;
vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2)
axis equal ; title('Filters in the first layer of the CNN') ;

%% -------------------------------------------------------------------------
% Part 4.5: apply the model
% -------------------------------------------------------------------------
% Get samples
%tstdb = getKaggleData_test();
%save('./data/kaggleImagesTest.mat','-struct','tstdb','-v7.3');

%tstdb = load('./data/kaggleImagesTest_eval.mat');

% Try other data,
%tstdb = getWhaleData(fileName);

% Perform click / sweep classification
% Extract just continous regions in the spectra
% Denoising, normalization, equalization, binariyation,
% Continous region detection
% - Directivity 
% - Frequency range

% Load the CNN learned before
net = load('data/whales-experiment/whalecnn_rev.mat');
net.layers{end+1} = struct('type', 'softmax');

tstdb.images.data = tstdb.images.data - net.imageMean ;

%vecPred=zeros(2,size(tstdb.images.id,2));

% Apply the CNN 
h = waitbar(0,'Please wait');
n = 1;
poll = cell(numel(tstdb.images.id));
scores = zeros(3,numel(tstdb.images.id));
for n=1:numel(tstdb.images.id),
    waitbar(n/numel(tstdb.images.id),h,'Please wait');
res = vl_simplenn(net, single(tstdb.images.data(:,:,n)),[],[]) ;
for i=1:size(res(end).x,2)
[score(i),pred(i)] = max(squeeze(res(end).x(1,i,:))) ;
end
poll{n} = res;
[scores(1,n) ii] = max(score);
scores(2,n) = pred(ii);
scores(3,n) = tstdb.images.label(n);
end
close(h)
save('./data/kagglePreResults.m','scores');
pause();

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
im = 256 * reshape(im, 128, 128, 1, []) ;
labels = imdb.images.label(1,batch) ;
