%This script attempts to demonstrate the use of CNNs as a sound
%enhancement method for ASR, noise reduction, etc.

addpath('../.');

%Load noisy speech signal

snDir='/home/obaldia/Proyectos/Sounds/Noizeus/';

flNme00='car/0dB/sp01_car_sn0.wav'; %0dB SNR
flNmeCln='clean/sp01.wav';  %A clean example
flNme15='car/15dB/sp01_car_sn15.wav' %15dB SNR

%Get signal and parameters
[x fs] = audioread(strcat(snDir,flNme00));
x=x(:,1);

%Perform noise reduction using the CNN
timeWindow = 0.050; %Window to extract the frames
hopSize = timeWindow.*fs; 

%This is the network for Image denoising
netDir = '../learned/weightsandbiases/';
thisNet = 'net-epoch-260_denoise2.mat'; 
load(strcat(netDir,thisNet));
net = vl_simplenn_tidy(net);
net.layers(end)=[];
net=vl_simplenn_tidy(net);
vl_simplenn_display(net)
addpath(netDir);

%First extract spectrogram chunks
winsize = 128;
noverlap = winsize-32;
nfft = 2048;

%Chunk size
chunkSize = winsize.*6; 
specWin = hann(ceil(chunkSize),'periodic');
[spIn F T] = spectrogram(x,winsize,noverlap,nfft);
spOut = complex(zeros(size(spIn)));

im = []; pin=0; pout=1;
pend = length(x)-length(specWin)-1;

% tic
% while pin < pend,
%    temp = x(pin+1:pin+length(specWin));
%    [sp F T] = spectrogram(temp,winsize,noverlap,nfft);
%     %Calculate power spectrogram
%     %spL = 20*log10(abs(sp));
%     spL = abs(sp);
%     %Create input
%     inSp = mat2gray(spL);
%     inSp = imresize(inSp,[300 500],'bicubic');
%     inSp = single(inSp);
%     inBW = edge(inSp,'log');
%     inOW = im2bw(inSp,0.8);
%     im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
%     %Send input to CNN
%     [res err1 err2 err3] = vl_simplenn_purba(net, single(im), 0, [],[],'mode','test');
%     denoised = (imresize(res(end).x,[size(sp,1),size(sp,2)]).*sp);
%     pin = pin + length(specWin) - winsize;
%     spOut(:,pout:pout+size(sp,2)-1)= denoised; %Do some smooth transitioning
%     pout = pout + 20;
% end
% toc

tic
chnkSize=300;
spOut2 = complex(zeros(size(spIn)));
pin = 0; pend = size(spIn,2)-chnkSize; pout=1;
while pin < pend,
    chunks = spIn(:,pin+1:pin+chnkSize);
    spL=abs(chunks);
    %Create input
    inSp = mat2gray(spL); inSp = inSp';
    inSp = imresize(inSp,[300 500],'bicubic');
    inSp = single(inSp);
    inBW = edge(inSp,'log');
    inOW = im2bw(inSp,0.9);
    im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
    %Send input to CNN
    [res err1 err2 err3] = vl_simplenn_regression(net, single(im), [], [],'mode','test');
    denoised = (imresize(res(end).x,[size(chunks,1),size(chunks,2)]).*chunks);
    spOut2(:,pout:pout+chnkSize-1)=denoised;
    pin = pin + chnkSize;
    pout = pin;
end
toc