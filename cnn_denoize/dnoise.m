sndFldr= '/home/obaldia/Proyectos/Whales/sounds/YouTube/';
fileName = 'NARW_up-call.wav';

% Current data
timeRes = 0.5; %The frame size in seconds
freqRes = 1000; %The frequency bandwidth

oLapF = 200;
oLapT = 0.75*timeRes; %in seconds... more than 75%?

% Read input signal
[a fs] = audioread(strcat(sndFldr, fileName));
a=a(:,1); %Take only left channel.
fsn = 2000;


a = resample(a,fsn,fs); fs = fsn;

% Calculate the spectrum
nfft_s = 4096;
win_s = 512;
olap_s = floor(win_s - win_s/8);
noiseTh = -45;

[sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
%spL = 20*log10(abs(sp)); %spLN = spL;
spL=abs(sp)./max(max(abs(sp)));
inSp = mat2gray(spL);
inSp = imresize(inSp,[300 500],'bicubic');
%spL(spL<=noiseTh) = noiseTh;

% Perform de-noising and superresolution.
netDir = 'weightsandbiases/';
thisNet = 'net-epoch-260_denoise2.mat';
load(strcat(netDir,thisNet));
net = vl_simplenn_tidy(net);
addpath(netDir);

inSp = single(inSp);
inBW = edge(inSp,'log');
inOW = im2bw(inSp,0.9);
im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
im = single(im);
net.layers(end)=[];
vl_simplenn_display(net)
[res err1 err2 err3] = vl_simplenn_purba(net, im, 0, [],[],'mode','test'); 

dmask=imresize(res(end).x./max(max(abs(res(end).x))),[size(sp,1),size(sp,2)]);
denoised = (dmask.*sp);

%out = specsynth(denoised);
out2=invspecgram(denoised./max(max(abs(denoised))),nfft_s,fs,win_s,olap_s);

figure;
subplot 121
imagesc(tp,fp,20*log10(abs(sp./max(max(abs(sp))))));
axis xy
title('Original');
xlabel('Time');
ylabel('Frequency');

subplot 122
imagesc(tp,fp,20*log10(abs(denoised./max(max(abs(denoised))))));
axis xy
title('Denoised');
xlabel('Time');
ylabel('f \rightarrow');