function despeech_train_all_0()


getdB=1;
%First step is to read the wavefiles and generate a imdb
if getdB,
    [files info] = medleydb();
end

g=1; collect = [];
for g = 1: size(files,2),
%Get images and labes for each chunk.
path1 = info{g}.dir;
mixedSnd = strcat(path1,files{g}.mix);

% Get the vocals
vocalsSnd = []; cnt = 0;
list = fieldnames(files{g});
for g0 = 1 : size(list),
    if strcmp(list{g0}(1),'S')
        cnt = cnt + 1;
        tmp = getfield(files{g},list{g0});
        vocalsSnd{cnt} = strcat(path1, info{files{g}.id}.stem_dir,'/',tmp);        
    end
end
[im] = makedata(mixedSnd, vocalsSnd);
collect{g} = im; clear im;
end

%Create imdb
imdb.data = []; n00=1;
for n0=1:size(collect,2),
        getIm = collect{n0};
        tmpcell = getIm{1};
        tmplbl = getIm{2};
        for n1=1:size(tmpcell,3),
            inSp = single(tmpcell(:,:,n1));
            inBW = edge(inSp,'log');
            inOW = im2bw(inSp,0.9);
            im(:,:,1)=inSp; im(:,:,2)=inOW; im(:,:,3)=inBW;
            imdb.images.data(:,:,:,n00)=im;
            imdb.images.id(n00) = n0;
            imdb.images.label(:,:,n00) = tmplbl(:,:,n1);
            imdb.images.set(n00) = 1;
            n00 = n00 + 1;
        end
end
%
% % Perform de-noising and superresolution.

%setup;

netDir = 'weightsandbiases/';
thisNet = 'net-epoch-260_denoise2.mat';
load(strcat(netDir,thisNet));
net = vl_simplenn_tidy(net);
addpath(netDir);
%

tic
trainOpts.batchSize = 5;
trainOpts.numEpochs = 1000;
trainOpts.continue = true ;
%trainOpts.residualnet = 1;
%trainOpts.useGpu = true ;
lr=0.1; %changed from 0.5 22.04 %0.0007 and 2.0004
for n=1:(trainOpts.numEpochs-1),
    lr=[lr lr(end)/1.00004];
end

%net.layers(end)=[];

[net,info] = cnn_train(net, imdb, @getBatch, trainOpts);
%
% trainOpts.learningRate = lr ;
% trainOpts.expDir = 'data/whales-experiment' ;
%
% disp('Training the network...');
%
% % Take the average image out
% imageMean = mean(imdb.images.data(:)) ;
% imdb.images.data = imdb.images.data - imageMean ; %THIS IS REDUNDANT WHATEVER
%
% imdb.images.data = single(imdb.images.data);
%
%
% toc
%
disp('Saving the net'); timeStamp = datestr(now,'mm-dd-HH-MM');
save(strcat('net-dnoiz-',tr,'-',timeStamp,'.mat'), '-struct', 'net') ;
cleanTrain;

end

function im = makedata(mixed, track)
%%
% Current data
timeRes = 0.5; %The frame size in seconds

nfft_s = 2048; win_s = 512; olap_s = floor(win_s - win_s/8);
noiseTh = -20;


% Read input signal
[a fs] = audioread(mixed);
a = a(:,1); %Take only left channel.
a = (a - mean(a))./std(a); a = a./max(abs(a));

%For the labels
tmp = zeros(size(a));


% Decimate to 16k fs
a = decimate(a, 4, 'fir');
fs_n = fs./4;



% Calculate the spectrum

[sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
spL=abs(sp)./max(max(abs(sp))); 
spL = 20*log10(abs(sp)); spL(spL<noiseTh)=noiseTh;

inSp = mat2gray(spL);

% Extract the images, 1024 times 300;
ix = 100; iy = 600; n1 = 1;
pin = 0; pend = size(inSp,2) - ix; ims=[];
while pin < pend,
    ims(:,:,n1) = imresize(inSp(:,pin+1:pin+ix),[ix iy]);
    pin = pin + ix; n1=n1+1;
end
im{1} = ims;

% Read label---------------------------------------------------------_...
for g=1:size(track,2),
% Acumulate Vocals
[a fs] = audioread(track{g});
a = a(:,1); %Take only left channel.
a = (a - mean(a))./std(a);
a = a./max(abs(a));
tmp = tmp + a;
end
disp(strcat(num2str(g),' signals with vocals loaded'));
a = tmp./max(abs(tmp));

% Decimate to 16k fs
a = decimate(a, 4, 'fir');
fs_n = fs./4;

[sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
spL=abs(sp)./max(max(abs(sp)));
spL = 20*log10(abs(sp)); spL(spL<noiseTh)=noiseTh;

inSp = mat2gray(spL);

% Extract the images, 1024 times 300;
ix = 100; iy = 600; n1 = 1;
pin = 0; pend = size(inSp,2) - ix; ims=[];
while pin < pend,
    ims(:,:,n1) = imresize(inSp(:,pin+1:pin+ix),[ix iy]);
    pin = pin + ix; n1=n1+1;
end

im{2} = ims;


end %makedata

function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
isRGB = false;

im = single(imdb.images.data(:,:,:,batch));
%im = imresize(im,[32 32 3]);
%im = reshape(im,32 ,32 , 3, []);
labels = imdb.images.label(:,:,batch);
labels=reshape(labels,size(labels,1),size(labels,2),1,[]);
end

function visdata(ims,lbs),

figure('Name','Signals')
for n=1:size(ims,3),
subplot 211
imagesc(ims(:,:,n))
axis xy
subplot 212
imagesc(lbs(:,:,n))
axis xy
pause(0.1)
end

end
