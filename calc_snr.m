function varargout = calc_snr(inS, inN,  fs, range)

if isempty(range),
    h=figure(88),
    plot (inS);
    title('Select silent region');
    [x y] = ginput(n);
    close(h);
    range = [x(1) x(2)]; range = [range;range];
end

inS = inS(:,1); inN = inN(:,1);
signal = inS(range(2,1):range(2,2));
noise = inN(range(1,1):range(1,2));

snr_range = mean(signal.^2)/mean(noise.^2);

varargout{1} = snr_range;

