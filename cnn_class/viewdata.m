function viewdata()
%View data for imdb

close all

load('../example_set/images_k2_gray_tier1.mat','imdb');

disp('Showing positive examples');
disp('Press any key to continue,');
disp('<ctrl-c> exits');

%Find images and respective mask
for n=1:size(imdb.images.id,1),
    if (imdb.images.label(1,n)==1),
        figure(1)
        subplot 121
        imagesc(imdb.images.data(:,:,n));
        axis xy
        
        subplot 122
        wavelength = 2;
        orientation = 90;
        [mag, phase] = imgaborfilt(imdb.images.data(:,:,n),wavelength,orientation);
        imagesc(mag);
        axis xy
       
        pause()
    end
end