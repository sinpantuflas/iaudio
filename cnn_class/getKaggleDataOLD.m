function varargout = getKaggleData()
% This function extracts whale spectrogram information for application of a
% CNN using the MATConvNet library. The output structure is a .mat


% Extract the waveform

noiseTh = -40;
imgSize = [64 64];

percentForValidation = 40;

percentForTraining = 30;

%TODO: NORMALIZE SPEC,
%USE 4 IMAGES PER 2 SEC CLIP FOR VARIABILITY (IMAGE_HOP)
%normalizeSpec

% Select the time slot, just for image_hop
%time_slot = 1;
%time_overlap = 0.5;

% Get samples
fileDir = '/home/obaldia/Proyectos/Whales/sounds/kaggle/train2/';
lsVec = ls(fileDir);
listFilesT = strread(lsVec,'%s','delimiter',' ');

%Initialize struct
imdb.images.id = 1:(length(listFilesT));
imdb.images.label = zeros(1,length(listFilesT));
imdb.images.set = zeros(1,length(listFilesT));
imdb.images.data = zeros(imgSize(1),imgSize(2),length(listFilesT));

% Read file
%n=1;
%There is an empty characther at the end of the first sample
listFilesT=strtrim(listFilesT);

h = waitbar(0,'Please wait');
for (n = 1: length(listFilesT)),
    waitbar(n/length(listFilesT),h)

[x fs] = audioread(strcat(fileDir,listFilesT{n}));

% Pre-filter 
order    = 10;
fcutlow  = 50;
fcuthigh = 650;
[bs,as]    = butter(order,[fcutlow,fcuthigh]/(fs/2), 'bandpass');
a        = filter(bs,as,x);


thisLabel = fliplr(listFilesT{n}(1:end));
thisLabel = thisLabel(5);
thisLabel = str2num(thisLabel) + 1;

[sp] = spectrogram(a,128,32,2048);
spL = 20*log10(abs(sp));
spL(spL<=noiseTh) = noiseTh;

% Scale to [0,1]
spLsc = spL - (min(min(spL)));
spLsc = spLsc/max(max(spLsc));

% Resize
spImg = imresize(spLsc,imgSize);



imdb.images.id(n)=n;
imdb.images.label(n)=thisLabel;
imdb.images.data(:,:,n)=spImg;

if (n >= ceil(length(listFilesT).*percentForValidation/100))
    imdb.images.set(n) = 2; %validate
else
    imdb.images.set(n) = 1;
end
end
close(h)

varargout{1} = imdb;

end




