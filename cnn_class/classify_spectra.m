function classify_spectra()
close all; clear all;
%% Try other data,

%sndFldr= '/home/obaldia/Proyectos/Whales/sounds/Neue_Sounds_2014-10-07/118_Großer Tümmler_2/';
%fileName = 'T_TRUNCATUS_WHISTLES.WAV';

sndFldr= '/home/obaldia/Proyectos/Whales/sounds/YouTube/';
fileName = 'NARW_up-call.wav';

% Current data
timeRes = 0.5; %The frame size in seconds
freqRes = 1000; %The frequency bandwidth

oLapF = 200;
oLapT = 0.75*timeRes; %in seconds... more than 75%?

% Read input signal
[a fs] = audioread(strcat(sndFldr, fileName));
a=a(:,1); %Take only left channel.
fsn = 2000;

a = resample(a,fsn,fs); fs = fsn;

% Calculate the spectrum
nfft_s = 4096;
win_s = 512;
olap_s = floor(win_s - win_s/8);
noiseTh = -45;

[sp fp tp] = spectrogram(a,win_s,olap_s,nfft_s, fs);
spL = 20*log10(abs(sp)); %spLN = spL;
inSp = mat2gray(spL);
inSp = imresize(inSp,[300 500],'bicubic');

% Create spectrum grid
rF=[]; rT=[];
s=0;
while s + freqRes <= fs/2;
    rF = [rF; s, s + freqRes];
    s = s + freqRes - oLapF;
end
s=0;
while s + timeRes <= tp(end);
    rT = [rT; s, s + timeRes];
    s = s + timeRes - oLapT;
end

rF = rF.*nfft_s./fs;
rTN = [];
for n=1:size(rT,1),
    fff0 = find(tp <= rT(n,1),1,'last');
    if isempty(fff0),
        fff0 = 1;
    end
    fff1 = find(tp >= rT(n,2),1,'first');
    rTN = [rTN; [fff0, fff1]];
end

figure(101),clf
subplot (211), imagesc(spL);
colormap gray; axis xy
hold on
for xxn = 1:size(rTN,1),
xxxx=[rTN(xxn,1), rTN(xxn,2), rTN(xxn,2), rTN(xxn,1), rTN(xxn,1)];
for yyn=1:size(rF,1),
yyyy=[rF(yyn,1), rF(yyn,1), rF(yyn,2), rF(yyn,2), rF(yyn,1)];
plot(xxxx,yyyy,'LineWidth',1);
end
end

maskSP = zeros(size(spL));

%Classify!
numbs=size(rTN,1)*size(rF,1);
polled=[];
listed=[];
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-gray-01/whales-experiment/whalecnn_rev.mat');
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-gray-02/whales-experiment/whalecnn_rev-1604061045.mat');
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-gray-02x/whales-experiment/whalecnn_rev-1604061704.mat');
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-gray-02x/whales-experiment/net-epoch-1.mat');
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-gray-03g/whales-experiment/whalecnn_rev-1604111211.mat');
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-gray-03g/whales-experiment/net-epoch-10.mat');
%net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalenet-04/data/whales-experiment/whalecnn_rev-1605041117.mat');
net = load('/home/obaldia/Proyectos/MLLab/cnn-whale-one/learnedNets/whalecnn.mat');
%net.layers{1}.filters=net.layers{1}.filters./max(max(max(abs(net.layers{1}.filters))));
%net.layers{1}.filters=0.04.*net.layers{1}.filters;%./max(max(max(abs(net.layers{1}.filters))));
%net.layers(2)=[];
%net.layers{1}.filters = (net.layers{1}.filters - mean(mean(mean(abs(net.layers{1}.filters)))))./std(std(std(abs(net.layers{1}.filters))));

%net = vl_simplenn_tidy(net);

L = 36;
w1D = window(@tukeywin,L); % Some 1D window
M = (L-1)/2;
xx = linspace(-M,M,L);
[x,y] = meshgrid(xx);
r = sqrt( x.^2 + y.^2 );
w2D = zeros(L);
w2D(r<=M) = interp1(xx,w1D,r(r<=M));

net.layers{end+1} = struct('type', 'softmax') ;
sz=[36 36];
nnnn=1;
h0=waitbar(0,'Evaluating');
for xxn = 1:size(rTN,1),
    for yyn = 1:size(rF,1),
        try
        waitbar(nnnn/numbs,h0);
        lowF = (floor(rF(yyn,1))+1); highF = ceil(rF(yyn,2));
        lowT = rTN(xxn,1); highT = rTN(xxn,2);
        specCutA = spL(lowF:highF,lowT:highT);
        specCut = mat2gray(specCutA);
        specCut = single(imresize(specCut, [sz(1) sz(2)]));
        specCut = specCut - net.imageMean;
        specCut = w2D.*specCut;
        opts.disableDropout = true;
        
        res = vl_simplenn(net, specCut,[],opts) ;
        %res = vl_simplenn(net, specCut,[],[],'mode','test') ;
        [e f] = max(res(end).x);
        
        if f==1 && e>0.85,
            disp(strcat('Sweep: ',num2str(e*100),'%'));
            
            prob = e* ones(size(specCutA));            
            maskSP(lowF:highF,lowT:highT) = prob + maskSP(lowF:highF,lowT:highT);
        end
        
        polled = [polled; [e, f]];
        listed = [listed; [xxn, yyn]];
        nnnn=nnnn+1;
        catch ME    
            disp(ME.identifier);
            disp(ME.message);
        end
    end
end
close(h0)

figure(101)
subplot(212);
h=imagesc(spL);
axis xy
set(h,'AlphaData',maskSP)