function cleanTrain

a = dir('data/');

try

for n = 3:size(a,1),
    if ~a(n).isdir,
    delete(strcat('data/', a(n).name));
    else
        rmdir(strcat('data/', a(n).name),'s');
    end
end

catch ME
    disp(ME.message);
end
