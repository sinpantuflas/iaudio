
function varargout = getKaggleData_tstv2_S(varargin)
% This function extracts whale spectrogram information for application of a
% CNN using the MATConvNet library. The output structure is a .mat
% Using Smirnov aspect ratio.

%if nargin<1,
%noiseTh = -35;
isRGB=varargin{1};
imgSize = [64 64 3];
%else
%noiseTh = -35;
%imgSize = varargin{1};
%end

noiseTh = -25;
%TODO: NORMALIZE SPEC,
%USE 4 IMAGES PER 2 SEC CLIP FOR VARIABILITY (IMAGE_HOP)
%normalizeSpec
fileDir = '/home/carlos/Projects/MLLab/snd/data/';

%isRGB = true;
clrmap=colormap(hsv); %a=colormap(hot);
%isRGB = false;


% Select the time slot, just for image_hop
%time_slot = 1;
%time_overlap = 0.5;

% Get samples
grndData=csvimport([fileDir,'train.csv']);
fileDir = '/home/carlos/Projects/MLLab/snd/data/train/';
grndLabl = grndData(2:end,2);
numT = find(cell2mat(grndLabl)== 1);
numF = find(cell2mat(grndLabl)== 0);

p_tst = 22;
p_val = 20;
p_trn = 58;

s_tst = floor(length(numT).*p_tst/100);
s_val = floor(length(numT).*p_val/100);
s_trn = floor(length(numT).*p_trn/100);

ix_tst = numT(1:s_tst);
ix_val = numT(s_tst + 1:s_tst + s_val);
ix_trn = numT(s_tst + s_val + 1:end);

ix_tstF = numF(1:s_tst);
ix_valF = numF(s_tst + 1:s_tst + s_val);
ix_trnF = numF(s_tst + s_val + 1:s_tst + s_val + s_trn);

ix_ts = [ix_tst;ix_tstF];
ix_vl = [ix_val;ix_valF];
ix_tr = [ix_trn;ix_trnF];

%Initialize struct for the training/validation set.
imdb.images.id = [ix_vl;ix_tr];
imdb.images.label = zeros(1,length(ix_vl)+length(ix_tr));
if isRGB,
  imdb.images.data = zeros(imgSize(1),imgSize(2),3,length(ix_vl)+length(ix_tr));
else
   tstdb.images.data = zeros(imgSize(1),imgSize(2),length(ix_ts));
end
%Initialize struct for the testing set.
tstdb.images.id = ix_ts;
tstdb.images.label = zeros(1,length(ix_ts));
if isRGB,
tstdb.images.data = zeros(imgSize(1),imgSize(2),3,length(ix_ts));
else
   tstdb.images.data = zeros(imgSize(1),imgSize(2),length(ix_ts));
end
% Read file
nn=1;
%There is an empty characther at the end of the first sample
tmp = grndData(2: end,1);
tmp = cellstr(tmp);
listFilesT=strtrim(tmp);

indexes = [ix_tr;ix_vl;ix_ts];

h = waitbar(0,'Please wait');
for (n = 1: length(indexes)),
    waitbar(n/length(indexes),h)
try
[a fs] = audioread(strcat(fileDir,listFilesT{indexes(n)}));
    thisLabel = grndLabl(indexes(n));
a = a(fix(0.05*fs):end,1); % take only one channel and skip first samples
a = a-mean(a); % make zero mean

%Filter around 50 and 650
% Pre-filter 
order    = 10;
fcutlow  = 50;
fcuthigh = 650;
[bs,as]    = butter(order,[fcutlow,fcuthigh]/(fs/2), 'bandpass');
a0        = filter(bs,as,a);

nfft_s = 4096;
win_s = 256;
olap_s = floor(win_s - win_s/8);

[sp] = spectrogram(a0,win_s,olap_s,nfft_s);
spL = 20*log10(abs(sp));
spL(spL<=noiseTh) = noiseTh;

spCut = spL(ceil(fcutlow*nfft_s/fs):ceil(fcuthigh*nfft_s/fs),:);
spTmp = spL;
spL = spCut;

% Scale to [0,1]
% spLsc = spL - (min(min(spL)));
% spLsc = spLsc/max(max(spLsc));

spImg = mat2gray(spL);
if isRGB,
[spImg ind] =gray2ind(spImg);
spImg = ind2rgb(spImg,clrmap);
end

% Resize
spImg = imresize(spImg,imgSize(1:2));

% Create test, validation and training sets.

if (n <= (length(ix_vl)+length(ix_tr))),
    
    imdb.images.id(n)=n;
    imdb.images.label(n)=cell2mat(thisLabel);
    if isRGB,
        imdb.images.data(:,:,:,n) = spImg;
    else
        imdb.images.data(:,:,n) = spImg; 
    end
    if (n >= (s_trn)),
        imdb.images.set(n) = 2; %validate
    else
        imdb.images.set(n) = 1;
    end
    
elseif (n <= length(ix_vl)+length(ix_tr)+length(ix_ts)),
    
    tstdb.images.id(nn)=n;
    tstdb.images.label(nn)=cell2mat(thisLabel);
    if isRGB,
        tstdb.images.data(:,:,:,nn) = spImg;
    else
        tstdb.images.data(:,:,nn) = spImg; 
    end
    tstdb.images.set(nn) = 3;
    nn = nn + 1;
else
    break;
end

catch ME
        warndlg(strcat('Skipping sound - ',listFilesT{n}));
        listFilesT{n}=[];
        n = n - 1;
end

end
close(h)

varargout{1} = imdb;
varargout{2} = tstdb;
end