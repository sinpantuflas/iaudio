
function net = initializeWhaleCNN_gabor2(isRGB)
% 02.2016 Carlos de Obaldia

if nargin < 1,
isRGB = false;
end

if isRGB
    firstDimension = 3;
else
    firstDimension = 1;
end

% 02.2016 http://www.cc.gatech.edu/~hays/compvision/results/proj6/dst3/index.html

f=1/100 ;
net.layers = {} ;


A=f*randn(6,6,1,48, 'single');
%
G2=zeros([6 6 1 48]);
for n=1:48,
G2(:,:,1,n)=G{n};
GR = real(G2);
end

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', single(GR), ...
                           'biases', zeros(1, 48, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;

%net.layers{end+1} = struct('type', 'dropout','rate', 0.8);                     

net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [4 4], ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(6,6,48,24, 'single'),...
                           'biases', zeros(1,24,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [4 4], ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(4,4,24,12, 'single'),...
                           'biases', zeros(1,12,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(18,18,12,2, 'single'),...
                           'biases', zeros(1,2,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'softmaxloss') ;


% A=f*randn(6,6,1,48, 'single');
% %
% G= gaborFilterBank(1,48,6,6);
% G2=zeros([6 6 1 48]);
% for n=1:48,
% G2(:,:,1,n)=G{n};
% GR = real(G2);
% end
% 
% figure(49)
% for n = 1:48,
% subplot(7,7,n)
% %imagesc(A(:,:,1,n));
% imagesc(real(G2(:,:,1,n)));
% axis off
% end
