function varargout = getKaggleData()
% This function extracts whale spectrogram information for application of a
% CNN using the MATConvNet library. The output structure is a .mat

noiseTh = -35;
imgSize = [64 64];

%TODO: NORMALIZE SPEC,
%USE 4 IMAGES PER 2 SEC CLIP FOR VARIABILITY (IMAGE_HOP)
%normalizeSpec

% Select the time slot, just for image_hop
%time_slot = 1;
%time_overlap = 0.5;

% Get samples
fileDir = '/home/obaldia/Proyectos/Whales/sounds/kaggle/train2/';
lsVec = ls(fileDir);
listFilesT = strread(lsVec,'%s','delimiter',' ');


p_tst = 20;
p_val = 24;
p_trn = 56;

s_tst = floor(length(listFilesT).*p_tst/100);
s_val = floor(length(listFilesT).*p_val/100);
s_trn = floor(length(listFilesT).*p_trn/100);

%Initialize struct for the training/validation set.
imdb.images.id = 1:(s_val + s_trn);
imdb.images.label = zeros(1,(s_val + s_trn));
imdb.images.data = zeros(imgSize(1),imgSize(2),(s_val + s_trn));

%Initialize struct for the testing set.
tstdb.images.id = 1:s_tst;
tstdb.images.label = zeros(1,s_tst);
tstdb.images.data = zeros(imgSize(1),imgSize(2),s_tst);

% Read file
nn=1;
%There is an empty characther at the end of the first sample
listFilesT=strtrim(listFilesT);

h = waitbar(0,'Please wait');
for (n = 1: length(listFilesT)),
    waitbar(n/length(listFilesT),h)
try
[a fs] = audioread(strcat(fileDir,listFilesT{n}));

a = a(fix(0.05*fs):end,1); % take only one channel and skip first samples
a = a-mean(a); % make zero mean

%Filter around 50 and 650
% Pre-filter 
order    = 10;
fcutlow  = 50;
fcuthigh = 650;
[bs,as]    = butter(order,[fcutlow,fcuthigh]/(fs/2), 'bandpass');
a0        = filter(bs,as,a);

nfft_s = 4096;
win_s = 256;
olap_s = floor(win_s - win_s/8);

thisLabel = fliplr(listFilesT{n}(1:end)); 
thisLabel = thisLabel(5);
thisLabel = str2num(thisLabel) + 1;

[sp] = spectrogram(a0,win_s,olap_s,nfft_s);
spL = 20*log10(abs(sp));
spL(spL<=noiseTh) = noiseTh;

spCut = spL(ceil(fcutlow*nfft_s/fs):ceil(fcuthigh*nfft_s/fs),:);
spTmp = spL;
spL = spCut;

% Scale to [0,1]
spLsc = spL - (min(min(spL)));
spLsc = spLsc/max(max(spLsc));

% Resize
spImg = imresize(spLsc,imgSize);

% Create test, validation and training sets.

if (n <= (s_trn + s_val)),
    imdb.images.id(n)=n;
    imdb.images.label(n)=thisLabel;
    imdb.images.data(:,:,n)=spImg;
    
    if (n >= (s_trn)),
        imdb.images.set(n) = 2; %validate
    else
        imdb.images.set(n) = 1;
    end
    
else 
    
    tstdb.images.id(nn)=n;
    tstdb.images.label(nn)=thisLabel;
    tstdb.images.data(:,:,nn)=spImg;
    tstdb.images.set(nn) = 3;
    nn = nn + 1;
end

catch ME
        warndlg(strcat('Skipping sound - ',listFilesT{n}));
        listFilesT{n}=[];
        n = n - 1;
end

end
close(h)

varargout{1} = imdb;
varargout{2} = tstdb;
end




