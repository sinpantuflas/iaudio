% Copyright (c) 2015, MathWorks, Inc.

%% Download and and predict using a pretrained ImageNet model

setup;

% Download from MatConvNet pretrained networks repository
%urlwrite('http://www.vlfeat.org/matconvnet/models/imagenet-vgg-f.mat', 'imagenet-vgg-f.mat');
cnnModel.net = load('imagenet-vgg-f.mat');

%% Load images from folder
imageSize = cnnModel.net.meta.normalization.imageSize;

isRGB=false;
%isRGB=false;

[imdb tstdb] = getKaggleData_testv2(isRGB);
%save('./Images_RGB_25dB.mat','imdb','tstdb');
%imdb=
%if isRGB,
%load('./Images_RGB_25dB.mat','imdb');
%else
%load('./Images_GRA_25dB.mat','imdb');
%end

% % Use imageSet to manage images stored in multiple folders
% imset = imageSet('pet_images','recursive');
% sa
% % Preallocate arrays with fixed size for prediction

trainingImages = zeros([64 64 3 numel(imdb.images.id)],'single');
for n = 1: numel(find(imdb.images.id)),
if ~isRGB
trainingImages(:,:,1,n) = single(imdb.images.data(:,:,n));
trainingImages(:,:,2,n) = single(imdb.images.data(:,:,n));
trainingImages(:,:,3,n) = single(imdb.images.data(:,:,n));%single(imresize(single(imdb.images.data(:,:,n)), imageSize(1:2)));
else
    trainingImages(:,:,:,n) = single(imdb.images.data(:,:,:,n));
end
end

%WITH cv

%trainingImages = load('./kaggleImages_resized_tr.mat');
% % Load and resize images for prediction
% for ii = 1:numel(imset)
%     for jj = 1:imset(ii).Count
%         trainingImages(:,:,:,jj) = imresize(single(read(imset(ii),jj)),imageSize(1:2));
%     end
% end
% 
% % Get the image labels directly from the ImageSet object
% trainingLabels = getImageLabels(imset);
% summary(trainingLabels)
trainingLabels = imdb.images.label;%(find(imdb.images.set==1));
clear imdb
%% Extract features using pretrained CNN

% Depending on how much memory you have on your GPU you may use a larger
% batch size. We have 400 images, I'm going to choose 200 as my batch size
cnnModel.info.opts.batchSize = 200;

% Make prediction on a CPU
[cnnFeatures] = cnnPredict2(cnnModel,trainingImages,'UseGPU',false);
% Make prediction on a GPU
%[cnnFeatures] = cnnPredict2(cnnModel,trainingImages,'UseGPU',true);
%save('./cnn_feature_vector.mat','cnnFeatures');
clear trainingImages;

a = size(cnnFeatures);
cnnFeature(a(3),a(4))=0;
cnnFeature(:,:)=cnnFeatures(1,1,:,:);
clear cnnFeatures;
% Compare the performance increase
%bar([sum(timeCPU),sum(timeGPU)],0.5)
%title(sprintf('Approximate speedup: %2.00f x ',sum(timeCPU)/sum(timeGPU)))
%set(gca,'XTickLabel',{'CPU','GPU'},'FontSize',18)
%ylabel('Time(sec)'), grid on, grid minor

%% Train a classifier using extracted features and calculate CV accuracy

% Train and validate a linear support vector machine (SVM) classifier.

% perform whitening http://neerajkumar.org/writings/svm/
wF = max(max(cnnFeature));
cnnFeature=cnnFeature./wF;

classifierModel = fitcsvm(cnnFeature', trainingLabels);

%save('./SVM_model.mat','classifierModel','-v7.3');

% 5 fold crossvalidation accuracy
%cvmdl = crossval(classifierModel,'KFold',5);
%fprintf('kFold CV accuracy: %2.2f\n',1-cvmdl.kfoldLoss)
%save('./mdl_imgnt17_GRAY_v2.mat','classifierModel');

%% Test on the images from the test set
%tstdb=
%load('./Images_GRA_25dB.mat','tstdb');

%% Object Detection
% Use findPet function that was automatically generated using the 
% Image Region Analyzer App
%without CV
%if isRGB,
%load('./Images_RGB_25dB.mat','tstdb');
%else
%load('./Images_GRA_25dB.mat','tstdb');
%end

tstimg = zeros([64 64 3 numel(tstdb.images.id)],'single');
for n = 1: numel(find(tstdb.images.id)),
if ~isRGB,
tstimg(:,:,1,n) = single(tstdb.images.data(:,:,n));
tstimg(:,:,2,n) = single(tstdb.images.data(:,:,n));
tstimg(:,:,3,n) = single(tstdb.images.data(:,:,n));%single(imresize(single(imdb.images.data(:,:,n)), imageSize(1:2)));
else
tstimg(:,:,:,n) = single(tstdb.images.data(:,:,:,n));
end
end

features = cell (numel (tstdb.images.label),1);
labels = cell (numel (tstdb.images.label),1);
for n = 1:(numel (tstdb.images.label))
       
       %GS
       img = tstimg(:,:,:,n);
       
      [tmp] = cnnPredict2(cnnModel,img,'UseGPU',false,'display',false);
      tmp2(1,size(tmp,3))=0;
      tmp2(1,:) = tmp(1,1,:);
      tmp2 = tmp2./wF; %whitening
      %features{n} = tmp2;
      labels{n} = predict(classifierModel,tmp2);
      %labels{n} = predict(cvmdl.Trained{end},features{n});
end

rGT=tstdb.images.label;
rLab=cell2mat(labels);
ConfusionMatrix=confusionmat(rGT,rLab')
CM=ConfusionMatrix; clear ConfusionMatrix
Accuracy=100*(CM(1,1)+CM(2,2))/sum(CM(:)) %Accuracy
Precision=100*(CM(2,2)/(CM(2,2)+CM(1,2))) %Precision (for clicks)
MissRate=100*(CM(2,1)/(CM(2,1)+CM(2,2))) %Miss Rate (for clicks)

pause()
%save('./SVM_results','features','labels','-v7.3');
% save('result_small_4ktrn_rgb_gs_tst.mat','rLab','rGT');
% 
% tmp2 = tmp(1,1,:);
% features{n} = tmp2';
% labels{n} = predict(classifierModel,features{n});
% end
% labels{n} = predict(classifierModel,features{n});
% features{1}
% clear tmp2
% tmp2(1,size(tmp,3))=0;
% tmp2 = tmp(1,1,:);
% size(tmp,3)
% clear tmp2;
% tmp2(1,size(tmp,3))=0;
% tmp2(1,:) = tmp(1,1,:);
% features{n} = tmp2;
% labels{n} = predict(classifierModel,features{n});
% labels{1}
% tstdb.images.label{1}
% tstdb.images.label(1)
% tstdb.images.label(2)
% clear features
% clear labels
% features = cell (numel (tstdb.images.label),1);
% labels = cell (numel (tstdb.images.label),1);
% for n = 1:(numel (tstdb.images.label))
% img = zeros(imageSize);
% img(:,:,1) = imresize(tstdb.images.data(:,:,n),imageSize(1:2));
% img(:,:,2) = img(:,:,1); img(:,:,3) = img(:,:,1);
% img = single(img);
% [tmp] = cnnPredict2(cnnModel,img,'UseGPU',false,'display',false);
% tmp2(1,size(tmp,3))=0;
% tmp2(1,:) = tmp(1,1,:);
% features{n} = tmp2;
% labels{n} = predict(classifierModel,features{n});
% end
% labels
% pl = cell2mat(labels);
% find(pl ==2)
% tstdb
% find(tstdb.images.label == 2)
% find(tstdb.images.label == 1)
% find(trainingLabels==2)
% a=find(trainingLabels==2);
% b=a;
% b=find(trainingLabels==1);
% b=a;
% c=find(trainingLabels==1);
% b=[b,c(1,1:length(a)+1)];
% b=sort(b);
% tLab = trainingLabels(b);
% cnnFe=cnnFeature(:,b);
% clasMod=fitcsvm(cnnFe',tLab);
% cvm=crossval(clasMod,'KFold',10);
% cvm
% clasMod
% clear features labels
% features = cell (numel (tstdb.images.label),1);
% labels = cell (numel (tstdb.images.label),1);
% for n = 1:(numel (tstdb.images.label))
% img = zeros(imageSize);
% img(:,:,1) = imresize(tstdb.images.data(:,:,n),imageSize(1:2));
% img(:,:,2) = img(:,:,1); img(:,:,3) = img(:,:,1);
% img = single(img);
% [tmp] = cnnPredict2(cnnModel,img,'UseGPU',false,'display',false);
% tmp2(1,size(tmp,3))=0;
% tmp2(1,:) = tmp(1,1,:);
% features{n} = tmp2;
% %labels{n} = predict(classifierModel,features{n});
% labels{n} = predict(clasMod,features{n});
% end
% a=find(tLab==1);
% size(a)
% a=find(tLab==2);
% size(a)
% rLab=cell2mat(labels);
% a=find(rLab==2);
% clear a
% a=find(rLab==2);
% a=find(rLab==1);
% a=find(tstdb.images.label==1);
% a=find(tstdb.images.label==2);
% save('./SVM_results','features','labels','-v7.3');
% rLab
% tLab
% rGT=tstdb.images.label;
% rGT=rGT';
% save('result_small_4ktrn.mat','rLab','rGT');
% confusionmat(rGT,rLab)
% size(rGT)
% 486+159/2000
% (486+159)/2000
% [cnnFeatures] = cnnPredict2(cnnModel,trainingImages,'UseGPU',false);
% clear cnnFe
% cnnFe=cnnFeature(:,b);
% clasMod=fitcsvm(cnnFe',tLab);
% cvm=crossval(clasMod,'KFold',10);
% clear features labels