function varargout = getKaggleData_tstv2_S(varargin)
% This function extracts whale spectrogram information for application of a
% CNN using the MATConvNet library. The output structure is a .mat
% This function uses a proven aspect ration for the images.

% In two seconds, 97 time bins and 72 frequency bins bewteen 50-650Hz. The
% kaggle signals are band-limited to 1kHz.


% This dataset: https://www.kaggle.com/c/whale-detection-challenge/data
% New dataset: https://www.kaggle.com/c/the-icml-2013-whale-challenge-right-whale-redux/data

if nargin>0,
    isRGB=varargin{1};
else
    isRGB = false;
end

%Changed output to 64
%%
if isRGB,
    imgSize = [72 97 3];
    clrmap=colormap(hsv); %a=colormap(hot);
else
    imgSize = [72 97 1];
end
%Changed to -45 from -25
noiseTh = -45; %Firs test using 25 noise threshold

sz=[62 62];

fileDir = '/home/obaldia/Proyectos/Whales/sounds/kaggle2/data/';        %
grndData = csvimport([fileDir,'train.csv']);                              %
fileDir = '/home/obaldia/Proyectos/Whales/sounds/kaggle2/data/train/';  %

grndLabl = grndData(2:end,2);
numT = find(cell2mat(grndLabl)== 1);
numF = find(cell2mat(grndLabl)== 0);

p_tst = 22;
p_trn = 78;

s_tst = floor(length(numT).*p_tst/100);
s_trn = floor(length(numT).*p_trn/100);

% The positive examples are repeated in the set
ix_tst = numT(1:s_tst);
ix_trn = numT(s_tst + 1:end);

% The negative examples are leaved as they are, since there are 3 times
% more negatives than positives.
ix_tstF = numF(1:3*s_tst);
ix_trnF = numF(3*(s_tst) + 1:end);

%%
%Initialize struct for the training set.
imdb.images.id = [repelem(ix_trn,3);ix_trnF];
imdb.images.idT = [repelem(ix_trn,3);ix_trnF];

imdb.images.label = zeros(1,length(imdb.images.id));
imdb.images.labelT = zeros(1,length(imdb.images.id));
imdb.images.labelT(1:3*length(ix_trn))=1;

if isRGB,
    imdb.images.data = zeros(sz(1),sz(2),3,length(imdb.images.id));
else
    imdb.images.data = zeros(sz(1),sz(2),length(imdb.images.id));
end

% Read file
nnn=1;
%There is an empty characther at the end of the first sample
tmp = grndData(2: end,1);
tmp = cellstr(tmp);
listFilesT=strtrim(tmp);

%----------------------------
% Select the time slot, just for image_hop
%time_slot = 1;
%time_overlap = 0.5;
%TODO: NORMALIZE SPEC,
%USE 4 IMAGES PER 2 SEC CLIP FOR VARIABILITY (IMAGE_HOP)
%normalizeSpec
%According to smirnov, the labeled set was
% Get samples

indexes_train = [ix_trn;ix_trnF];
indexes_test = [ix_tst;ix_tstF];

h = waitbar(0,'Please wait... Creating Training Set');
for (n = 1: length(indexes_train)),
    waitbar(n/length(indexes_train),h)
    try
        [a fs] = audioread(strcat(fileDir,listFilesT{indexes_train(n)}));
        thisLabel = grndLabl(indexes_train(n));
        a = a(fix(0.05*fs):end,1); % take only one channel and skip first samples
        a = a-mean(a); % make zero mean
        
        %Filter around 50 and 650
        % Pre-filter
        order    = 10;
        fcutlow  = 50;
        fcuthigh = 650;
        [bs,as]    = butter(order,[fcutlow,fcuthigh]/(fs/2), 'bandpass');
        a0        = filter(bs,as,a);
        
        nfft_s = 4096;
        win_s = 256;
        olap_s = floor(win_s - win_s/8);
        
        [sp] = spectrogram(a0,win_s,olap_s,nfft_s);
        spL = 20*log10(abs(sp));
        spL(spL<=noiseTh) = noiseTh;
        
        spCut = spL(ceil(fcutlow*nfft_s/fs):ceil(fcuthigh*nfft_s/fs),:);
        spTmp = spL;
        spL = spCut;
        
        % Scale to [0,1]
        % spLsc = spL - (min(min(spL)));
        % spLsc = spLsc/max(max(spLsc));
        
        spImg = mat2gray(spL);
        if isRGB,
            [spImg ind] =gray2ind(spImg);
            spImg = ind2rgb(spImg,clrmap);
        end
        
        % Resize
        spImg = imresize(spImg,imgSize(1:2),'bilinear');
        
        if cell2mat(thisLabel)==1,
            if isRGB,
                imdb.images.data(:,:,:,nnn) = imresize(spImg(1:72,1:72,:),sz,'bilinear');
                imdb.images.data(:,:,:,nnn+1) = imresize(spImg(1:72,13:84,:),sz,'bilinear');
                imdb.images.data(:,:,:,nnn+2) = imresize(spImg(1:72,26:97,:),sz,'bilinear');
            else
                imdb.images.data(:,:,nnn) = imresize(spImg(1:72,1:72),sz,'bilinear');
                imdb.images.data(:,:,nnn+1) = imresize(spImg(1:72,13:84),sz,'bilinear');
                imdb.images.data(:,:,nnn+2) = imresize(spImg(1:72,26:97),sz,'bilinear');
            end
            imdb.images.label(nnn:nnn+2)=repelem(cell2mat(thisLabel),3);
            imdb.images.id(nnn:nnn+2)=[indexes_train(n);indexes_train(n);indexes_train(n)];
            nnn=nnn+3;
        else
            
            if isRGB,
                imdb.images.data(:,:,:,nnn) = imresize(spImg(1:72,1:72,:),sz);
            else
                imdb.images.data(:,:,nnn) = imresize(spImg(1:72,1:72),sz);
            end
            imdb.images.label(nnn)=cell2mat(thisLabel);
            imdb.images.id(nnn)=indexes_train(n);
            nnn=nnn+1;
        end
    catch ME
        warndlg(strcat('Skipping sound - ',listFilesT{n}));
        listFilesT{n}=[];
        n = n - 1;
    end
end
close(h)
% Create validation set.
percentVal =40;
ix=randperm(length(imdb.images.id),floor(length(imdb.images.id).*percentVal/100));
imdb.images.set = ones(1,length(imdb.images.id));
imdb.images.set(ix) = 2;



%Create Test Set
%Initialize struct for the testing set.
tstdb.images.id = [repelem(ix_tst,3);ix_tstF];
tstdb.images.label = zeros(1,length(tstdb.images.id));
tstdb.images.labelT = zeros(1,length(tstdb.images.id));
imdb.images.labelT(1:3*length(ix_trn))=1;
nn=1;
if isRGB,
    tstdb.images.data = zeros(sz(1),sz(2),3,length(tstdb.images.id));
else
    tstdb.images.data = zeros(sz(1),sz(2),length(tstdb.images.id));
end
h = waitbar(0,'Please wait... Creating Test Set');
for (n = 1: length(indexes_test)),
    waitbar(n/length(indexes_test),h)
    try
        [a fs] = audioread(strcat(fileDir,listFilesT{indexes_test(n)}));
        thisLabel = grndLabl(indexes_test(n));
        a = a(fix(0.05*fs):end,1); % take only one channel and skip first samples
        a = a-mean(a); % make zero mean
        
        %Filter around 50 and 650
        % Pre-filter
        order    = 10;
        fcutlow  = 50;
        fcuthigh = 650;
        [bs,as]    = butter(order,[fcutlow,fcuthigh]/(fs/2), 'bandpass');
        a0        = filter(bs,as,a);
        
        nfft_s = 4096;
        win_s = 256;
        olap_s = floor(win_s - win_s/8);
        
        [sp] = spectrogram(a0,win_s,olap_s,nfft_s);
        spL = 20*log10(abs(sp));
        spL(spL<=noiseTh) = noiseTh;
        
        spCut = spL(ceil(fcutlow*nfft_s/fs):ceil(fcuthigh*nfft_s/fs),:);
        spTmp = spL;
        spL = spCut;
        
        % Scale to [0,1]
        % spLsc = spL - (min(min(spL)));
        % spLsc = spLsc/max(max(spLsc));
        
        spImg = mat2gray(spL);
        if isRGB,
            [spImg ind] =gray2ind(spImg);
            spImg = ind2rgb(spImg,clrmap);
        end
        
        % Resize
        spImg = imresize(spImg,imgSize(1:2),'bilinear');
        
        if cell2mat(thisLabel)==1,
            if isRGB,
                tstdb.images.data(:,:,:,nn) = imresize(spImg(1:72,1:72,:),sz,'bilinear');
                tstdb.images.data(:,:,:,nn+1) = imresize(spImg(1:72,13:84,:),sz,'bilinear');
                tstdb.images.data(:,:,:,nn+2) = imresize(spImg(1:72,26:97,:),sz,'bilinear');
            else
                tstdb.images.data(:,:,nn) = imresize(spImg(1:72,1:72),sz,'bilinear');
                tstdb.images.data(:,:,nn+1) = imresize(spImg(1:72,13:84),sz,'bilinear');
                tstdb.images.data(:,:,nn+2) = imresize(spImg(1:72,26:97),sz,'bilinear');
            end
            tstdb.images.label(nn:nn+2)=repelem(cell2mat(thisLabel),3);
            imdb.images.id(nn:nn+2)=[indexes_test(n);indexes_test(n);indexes_test(n)];
            nn=nn+3;
        else
            if isRGB,
                tstdb.images.data(:,:,:,nn) = imresize(spImg(1:72,1:72,:),sz);
            else
                tstdb.images.data(:,:,nn) = imresize(spImg(1:72,1:72),sz);
            end
            tstdb.images.id(nn)=indexes_test(n);
            tstdb.images.label(nn)=cell2mat(thisLabel);
            nn = nn + 1;
        end
        
    catch ME
        warndlg(strcat('Skipping sound - ',listFilesT{n}));
        listFilesT{n}=[];
        n = n - 1;
    end
end
close(h)

varargout{1} = imdb;
varargout{2} = tstdb;
end

function res=repelem(a,n) %To make it compatible with 2015a
a=a';
res = kron(a,ones(1,n));
res = res';
end

%%
