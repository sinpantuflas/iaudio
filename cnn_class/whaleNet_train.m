function whaleNet_train(varargin)
% We are using CNN for training a whale net that will catch all whales!

addpath(genpath('./data/'));
addpath(genpath('./vlfeat/'));
addpath(genpath('./matconvnet/'));

mkdir('./data/');

%Create date string
timeStamp = datestr(now,'yymmddHHMM');

setup;

isRGB = false;        % Flag to use RGB images as training examples
hasGUI = false;       % Flag to enable graphics

% --- 31.03 Training with kaggleData v2 using translated images.
if ~isRGB,
load('../example_set/images_k2_gray_tier1.mat','imdb');
else
% --- 05.04 Training with kaggleData v2 using translated images (RGB).
load('../example_set/images_k2_rgb_tier1.mat','imdb');
end
% In order to train using the sofmaxloss layer, the labels should be 1,2,
% etc. For binary classification the labels could be 0, 1, but the error
% function in cnn_train should be changed.
imdb.images.label(find(imdb.images.label==0))=2;

%Shuffle
shuffleSet = true;
if shuffleSet,
ix=randperm(length(imdb.images.label));
imdb.images.id=imdb.images.id(ix);
imdb.images.set=imdb.images.set(ix);
imdb.images.label=imdb.images.label(ix);
if ~isRGB,
imdb.images.data=imdb.images.data(:,:,ix);
else
imdb.images.data=imdb.images.data(:,:,:,ix);
end
save(strcat('./data/','indexed_terms_imdb-',timeStamp,'.mat'),'ix');
end

%net = initializeWhaleCNN_smirnov(isRGB);
%net = initializeWhaleCNN_tst(isRGB);
%net = initializeWhaleCNN_gabor_0(isRGB);
%net = initializeWhaleCNN_gabor2(isRGB);
net = initializeWhaleCNN_gabor(isRGB);
%net = initializeWhaleCNN_smirnov_nodo(isRGB);
%pause()
%imdb.images = normalizeDimensions(imdb.images);

vl_simplenn_display(net);

% Spatial labeling for regression. This should be better studied.
isSpatialLabel = false;
if isSpatialLabel,
libl = zeros(11,11,numel(imdb.images.id));
ELF=ones(11,11);
ix1=find(imdb.images.label==1);
for n=1:length(ix1),
libl(:,:,ix1(n))=1.*ELF;
end
ix0=find(imdb.images.label==2);
for n=1:length(ix0),
libl(:,:,ix0(n))=2.*ELF;
end
libl = reshape(libl, [11 11 1 numel(imdb.images.id)]);
imdb.images.label=libl;
end


% -------------------------------------------------------------------------
% Part 4.3: train and evaluate the CNN
% -------------------------------------------------------------------------
tic
trainOpts.batchSize = 100;
trainOpts.numEpochs = 25;
trainOpts.continue = true ;
%trainOpts.useGpu = false ;
lr=0.05;
for n=1:(trainOpts.numEpochs-1),
lr=[lr lr(end)/1.10004];
end

trainOpts.learningRate = lr ;
trainOpts.expDir = 'data/whales-experiment' ;
%trainOpts = vl_argparse(trainOpts, varargin);

% Take the average image out
imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ; %THIS IS REDUNDANT WHATEVER

% Convert to a GPU array if needed
%if trainOpts.useGpu
%  imdb.images.data = gpuArray(imdb.images.data) ;
%end

%Convert images to single
imdb.images.data = single(imdb.images.data);

%********* CDO: ERROR
% Error in stroke_nn_medicalData_011415 (line 76)
% [net, info] = cnn_train(net, medical_record_imdb, @getBatch, ...
% >>>> A better way is to set labels as +1 and -1, and set opts.errorFunction = 'binary' ; in the cnn_train file.

% Call training function in MatConvNet 
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts) ;
%[net,info] = cnn_train(net, imdb, @getBatchWithJitter, trainOpts) ;

toc
% Move the CNN back to the CPU if it was trained on the GPU
%if trainOpts.useGpu
%  net = vl_simplenn_move(net, 'cpu') ;
%end

disp('Saving the net');

%% Save the result for later use
net.layers(end) = [] ;
net.imageMean = imageMean;
save(strcat('data/whales-experiment/whalecnn_rev-',timeStamp,'.mat'), '-struct', 'net') ;

% -------------------------------------------------------------------------
% Part 4.4: visualize the learned filters
% -------------------------------------------------------------------------

%figure(2) ; clf ; colormap gray ;
%vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2)
%axis equal ; title('Filters in the first layer of the CNN') ;

%% -------------------------------------------------------------------------
% Part 4.5: apply the model
% -------------------------------------------------------------------------
% Get samples
%tstdb = getKaggleData_test();
%save('./data/kaggleImagesTest.mat','-struct','tstdb','-v7.3');

%tstdb = load('./data/kaggleImagesTest_eval.mat');

% Try other data,
%tstdb = getWhaleData(fileName);

% Perform click / sweep classification
% Extract just continous regions in the spectra
% Denoising, normalization, equalization, binariyation,
% Continous region detection
% - Directivity 
% - Frequency range

%pause()


% Load the CNN learned before
net = load(strcat('data/whales-experiment/whalecnn_rev-',timeStamp,'.mat')) ;
net.layers{end+1} = struct('type', 'softmax') ;

if ~isRGB,
load('../example_set/images_k2_gray_tier1.mat','tstdb');
else
load('../example_set/images_k2_rgb_tier1.mat','tstdb');
end
tstdb.images.data = tstdb.images.data - net.imageMean ;
tstdb.images.label(find(tstdb.images.label==0))=2;
%vecPred=zeros(2,size(tstdb.images.id,2));

% Apply the CNN 
%h = waitbar(0,'Please wait');
disp('Performing classification...');


%clear opts
%opts.mode = test;

n = 1;
poll = cell(numel(tstdb.images.id),1);
for n=1:numel(tstdb.images.id),
             %   waitbar(n/numel(tstdb.images.id),h,'Please wait');
    %res = vl_simplenn(net, single(tstdb.images.data(:,:,n)),[],[]) ;
    %res = vl_simplenn(net, single(imresize(tstdb.images.data(1:72,1:72,n),[36 36])),[],[]) ;
    if ~isRGB,
    %res = vl_simplenn(net, single(imresize(tstdb.images.data(:,:,n),[36 36])),[],[]) ;
    res = vl_simplenn(net, single(tstdb.images.data(:,:,n)),[],[],'mode','test') ;
    else
    res = vl_simplenn(net, single(tstdb.images.data(:,:,:,n)),[],[]) ;
    end
    %[e f] = max(max(max(res(end).x)));
    [e f] = max(res(end).x);
    poll{n} = [e, f];
end
%close(h)
r=cell2mat(poll);
rGT=tstdb.images.label;
rGT(rGT==0)=2;
rTS=r(:,2);
 
save(strcat('./data/result_map-',timeStamp,'.mat'),'poll','rGT','rTS');

% Confusion Matrix where 1 is the positive class (sweeps)
ConfusionMatrix=confusionmat(rGT,double(rTS'))
CM=ConfusionMatrix; clear ConfusionMatrix
Accuracy=100*(CM(1,1)+CM(2,2))/sum(CM(:)) %Accuracy
Precision=100*(CM(2,2)/(CM(2,2)+CM(1,2))) %Precision (for clicks)
MissRate=100*(CM(2,1)/(CM(2,1)+CM(2,2))) %Miss Rate (for clicks)

% Roc where 1 is the positive class (sweeps)

rr = zeros(size(rGT));
for m = 1: size(rGT),
    if rGT(m) == r(m,2)
    rr(m) = r(m,1);
    else
        rr(m) = 1 - r(m,1);
    end
end

[X, Y, T, AUC, ROCPT] = perfcurve(rGT,rr,1);

figure(2)
plot(X,Y,'LineWidth',2);
title('ROC Curve for Sweep Classification');
xlabel('False positive rate');
ylabel('True positive rate');
hold on
plot(ROCPT(1),ROCPT(2),'ro');
AreaUnderCurve = AUC*100

%pause();

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
isRGB = false;

if ~isRGB,
im = imdb.images.data(:,:,batch) ;
im = reshape(im, 36, 36, 1, []) ;%
else
im = imdb.images.data(:,:,:,batch) ;
im = reshape(im, 36, 36, 3, []) ;%
end

labels = imdb.images.label(1,batch) ;